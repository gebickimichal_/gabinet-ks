/* eslint-disable no-undef */
// eslint-disable-next-line no-unused-vars
export const initMap = () => {
    var myLatLng = {
        lat: 53.783574,
        lng: 20.496461,
    };

    const map = new google.maps.Map(document.getElementById('map'), {
        center: myLatLng,
        zoom: 16,
        disableDefaultUI: true,
        styles: 

        [
            {
              "elementType": "geometry",
              "stylers": [
                {
                  "color": "#f3f3f3",
                },
              ],
            },
            {
              "elementType": "labels.icon",
              "stylers": [
                {
                  "visibility": "off",
                },
              ],
            },
            {
              "elementType": "labels.text.fill",
              "stylers": [
                {
                  "color": "#383838",
                },
              ],
            },
            {
              "elementType": "labels.text.stroke",
              "stylers": [
                {
                  "color": "#f5f5f5",
                },
              ],
            },
            {
              "featureType": "administrative.land_parcel",
              "elementType": "labels.text.fill",
              "stylers": [
                {
                  "color": "#bdbdbd",
                },
              ],
            },
            {
              "featureType": "administrative.neighborhood",
              "elementType": "labels.text.fill",
              "stylers": [
                {
                  "color": "#5f5f5f",
                },
                {
                  "lightness": 45,
                },
                {
                  "visibility": "off",
                },
              ],
            },
            {
              "featureType": "poi",
              "elementType": "geometry",
              "stylers": [
                {
                  "color": "#e8e8e8",
                },
              ],
            },
            {
              "featureType": "poi",
              "elementType": "labels.text.fill",
              "stylers": [
                {
                  "color": "#757575",
                },
              ],
            },
            {
              "featureType": "poi.government",
              "elementType": "geometry.fill",
              "stylers": [
                {
                  "visibility": "off",
                },
              ],
            },
            {
              "featureType": "poi.government",
              "elementType": "geometry.stroke",
              "stylers": [
                {
                  "color": "#9e9e9e",
                },
              ],
            },
            {
              "featureType": "poi.park",
              "elementType": "geometry",
              "stylers": [
                {
                  "color": "#e8f1e8",
                },
                {
                  "visibility": "off",
                },
              ],
            },
            {
              "featureType": "poi.park",
              "elementType": "labels.text.fill",
              "stylers": [
                {
                  "color": "#9e9e9e",
                },
                {
                  "visibility": "off",
                },
              ],
            },
            {
              "featureType": "poi.school",
              "stylers": [
                {
                  "visibility": "off",
                },
              ],
            },
            {
              "featureType": "road",
              "elementType": "geometry",
              "stylers": [
                {
                  "color": "#ffffff",
                },
              ],
            },
            {
              "featureType": "road.arterial",
              "elementType": "geometry.stroke",
              "stylers": [
                {
                  "color": "#e6e6e6",
                },
              ],
            },
            {
              "featureType": "road.arterial",
              "elementType": "labels.text.fill",
              "stylers": [
                {
                  "color": "#408e42",
                },
              ],
            },
            {
              "featureType": "road.highway",
              "elementType": "geometry",
              "stylers": [
                {
                  "color": "#dadada",
                },
              ],
            },
            {
              "featureType": "road.highway",
              "elementType": "geometry.stroke",
              "stylers": [
                {
                  "color": "#cacaca",
                },
                {
                  "visibility": "off",
                },
              ],
            },
            {
              "featureType": "road.highway",
              "elementType": "labels.text.fill",
              "stylers": [
                {
                  "color": "#408e42",
                },
              ],
            },
            {
              "featureType": "road.local",
              "elementType": "geometry.stroke",
              "stylers": [
                {
                  "color": "#e1e1e1",
                },
              ],
            },
            {
              "featureType": "road.local",
              "elementType": "labels.text.fill",
              "stylers": [
                {
                  "color": "#9e9e9e",
                },
              ],
            },
            {
              "featureType": "transit",
              "elementType": "geometry",
              "stylers": [
                {
                  "color": "#a0d3a2",
                },
              ],
            },
            {
              "featureType": "transit",
              "elementType": "labels.text",
              "stylers": [
                {
                  "color": "#408e42",
                },
                {
                  "visibility": "simplified",
                },
              ],
            },
            {
              "featureType": "transit.line",
              "elementType": "geometry",
              "stylers": [
                {
                  "color": "#bbd7bb",
                },
              ],
            },
            {
              "featureType": "transit.station",
              "elementType": "geometry",
              "stylers": [
                {
                  "color": "#eeeeee",
                },
              ],
            },
            {
              "featureType": "transit.station.bus",
              "elementType": "labels.icon",
              "stylers": [
                {
                  "color": "#408e42",
                },
                {
                  "visibility": "off",
                },
              ],
            },
            {
              "featureType": "water",
              "elementType": "geometry",
              "stylers": [
                {
                  "color": "#77ac77",
                },
              ],
            },
            {
              "featureType": "water",
              "elementType": "labels.text.fill",
              "stylers": [
                {
                  "color": "#497849",
                },
              ],
            },
          ],
    });

    const icons = {
        marker_icon: {
            icon: {
                url: window.map_data.marker,
            },
        },
    };

    const features = [
        {
            position: new google.maps.LatLng(myLatLng.lat, myLatLng.lng),
            type: 'marker_icon',
        },
    ];


    // Create markers.
    features.forEach(function (feature) {
        new google.maps.Marker({
            position: feature.position,
            icon: icons[feature.type].icon,
            map,
        });
    });
}
