export const initValidation = () => {
    $( '.require-this' ).bind( 'change keyup focusout', function () {
        let $this = $( this );
        let min = parseInt( $( this ).data( 'r_min' ) );
        let max = parseInt( $( this ).data( 'r_max' ) );
        let visible = $( this ).is( ':visible' );
        if ( $this.val().length <= min && visible ) {
            $this.data( 'validity', 'BAD' );
            $( this ).parent().find( '.val-text' ).addClass( 'validation-span-hide' );
            $( this ).parent().find( '.val-text' ).text( $( this ).data( 'r_text' ) );
            $this.removeClass( 'anim-typewriter' ).addClass( 'anim-typewriter-hide' );

        } else if ( $this.val().length >= max && visible ) {
            $this.data( 'validity', 'GOOD' );
            $this.removeClass( 'anim-typewriter-hide' ).addClass( 'anim-typewriter' );
            $( this ).parent().find( '.val-text' ).removeClass( 'validation-span-hide' );
            $( this ).parent().find( '.val-text' ).text( '' );
        }
        else if ( !visible ) {
            $this.data( 'validity', '' );
            $( this ).parent().find( '.val-text' ).removeClass( 'validation-span-hide' );
            $this.removeClass( 'anim-typewriter-hide' ).removeClass( 'anim-typewriter' );
            $( this ).parent().find( '.val-text' ).text( '' );
        }
    } );

    // Validation of e-mail address
    $( '[type="email"]' ).bind( 'change keyup focusout', function () {
        var $INPUT1 = $( this );
        if ( $INPUT1.val() == '' || !isEmail( $INPUT1.val() ) ) {
            $INPUT1.data( 'validity', 'BAD' );
            $( this ).parent().find( '.val-text' ).addClass( 'validation-span-hide' );
            $( this ).parent().find( '.val-text' ).text( 'wprowadź prawidłowy adres email' );
            $INPUT1.removeClass( 'anim-typewriter' ).addClass( 'anim-typewriter-hide' );
        } else {
            $INPUT1.data( 'validity', 'GOOD' );
            $( this ).parent().find( '.val-text' ).text( '' );
            $INPUT1.removeClass( 'anim-typewriter-hide' ).addClass( 'anim-typewriter' );
            $( this ).parent().find( '.val-text' ).removeClass( 'validation-span-hide' );
        }
    } );

    function isEmail( email ) {
        var regex = /^([a-zA-Z0-9_.+-])+@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test( email );
    }

    $( 'input, textarea' ).bind( 'change keyup', function () {
        if (
            (($( 'input' ).data( 'validity' ) == 'BAD') || ($( 'textarea' ).data( 'validity' ) == 'BAD'))
            ||
            ($( 'input' ).hasClass( 'anim-typewriter-hide' ) || $( 'textarea' ).hasClass( 'anim-typewriter-hide' ))
        ) {
            // $( '#sendContactForm' ).attr( 'disabled', true );
            // console.log('test');
            window.isFormValid = false;
        } else {
            // $( '#sendContactForm' ).attr( 'disabled', false );
            // console.log('test');
            window.isFormValid = true;
        }
    } );


    $( '#sendContactForm' ).click( function () {
        $( '#sendContactForm' ).addClass( 'onclic', 250 );

        setTimeout( function () {
            $( '#sendContactForm' ).removeClass( 'onclic' );
            $( '#sendContactForm' ).addClass( 'validate', 450 );
        }, 2250 );

        setTimeout( function () {
            $( '#sendContactForm' ).removeClass( 'validate' );
        }, 4250 );
    } );
}
