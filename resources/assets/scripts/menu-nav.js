document.addEventListener("DOMContentLoaded", function() {

    window.mainHeader = document.getElementsByClassName("hide-header");

    window.belowNavHeroContent = document.getElementsByClassName("sub-nav-hero");

    window.headerHeight = window.mainHeader.clientHeight;

    //set scrolling variables
    window.scrolling = false;
    window.previousTop = 0;
    window.currentTop = 0;
    window.scrollDelta = 10;
    window.scrollOffset = 150;

    window.addEventListener('scroll', function() {
        if (!window.scrolling) {
            window.scrolling = true;
            (!window.requestAnimationFrame) ?
            window.setTimeout(window.autoHideHeader, 250): window.requestAnimationFrame(window.autoHideHeader);
        }
    });

    window.onresize = function() {
        window.headerHeight = window.mainHeader.innerHTML = window.innerHeight;
    };

    window.addClass = function(selector, myClass) {

        // get all elements that match our selector
        window.elements = document.querySelectorAll(selector);

        // add class to all chosen elements
        for (var i = 0; i < window.elements.length; i++) {
            window.elements[i].classList.add(myClass);
        }
    }

    window.removeClass = function(selector, myClass) {

        // get all elements that match our selector
        window.elements = document.querySelectorAll(selector);

        // remove class from all chosen elements
        for (var i = 0; i < window.elements.length; i++) {
            window.elements[i].classList.remove(myClass);
        }
    }

    window.autoHideHeader = function() {
        var currentTop = window.scrollY;

        if (currentTop > 0) {
            window.addClass('.hide-header', 'visible-bg');
        } else {
            window.removeClass('.hide-header', 'visible-bg');
        }

        (window.belowNavHeroContent.length > 0) ?
        window.checkStickyNavigation(currentTop) // secondary navigation below intro
            : window.checkSimpleNavigation(currentTop);

        window.previousTop = currentTop;
        window.scrolling = false;
    }

    window.checkSimpleNavigation = function(currentTop) {
        //there's no secondary nav or secondary nav is below primary nav
        if (window.previousTop - currentTop > window.scrollDelta) {
            //if scrolling up...
            window.removeClass('.hide-header', 'is-hidden');

        } else if (currentTop - window.previousTop > window.scrollDelta && currentTop > window.scrollOffset) {
            //if scrolling down...
            window.addClass('.hide-header', 'is-hidden');
        }
    }

});

jQuery( window ).on( 'load', function () {
  jQuery( '#overlay' ).fadeOut();
} );
