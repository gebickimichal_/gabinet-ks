/**
 * External Dependencies
 */
import 'jquery';
import UIkit from 'uikit';
import Icons from 'uikit/dist/js/uikit-icons';
import { router } from 'js-dom-router';

/**
 * Utils
 */
import { ready } from "./utils";

/**
 * DOM-based routing
 */
import kontakt from './routes/kontakt';
import faq from './routes/faq';
import analizaSkladuCiala from './routes/analizaSkladuCiala';
import przedWizyta from './routes/przedWizyta';
import uslugi from './routes/uslugi';
import oMnie from './routes/oMnie';
import custom from "./custom";

/**
 * Set up DOM router
 *
 * .on(<name of body class>, callback)
 *
 * See: {@link http://goo.gl/EUTi53 | Markup-based Unobtrusive Comprehensive DOM-ready Execution} by Paul Irish
 */
router
    .on('analiza-skladu-ciala', analizaSkladuCiala)
    .on('faq', faq)
    .on('przed-wizyta', przedWizyta)
    .on('uslugi', uslugi)
    .on('kontakt', kontakt)
    .on('o-mnie', oMnie)
    .ready();

UIkit.use( Icons );
ready(custom);