import { initValidation } from "../validation";
import SlideToUnlock from "../jquery.slideToUnlock";
import swal from "sweetalert";
import {showContactFormProcessingSpinner, hideContactFormProcessingSpinner} from '../sendFormButtonSpinner';

/**
 * Home
 */
export default () => {
    initValidation();
    initalizeSlider();

    $('#contactForm').on('submit', e => {
        e.preventDefault();

        $( '#contactForm' ).find( 'input, textarea' ).trigger( 'change' );
        let $validate = $( '#contactForm' ).find( '[validity="BAD"]' );
        if ( $validate.length ) return;

        if ( $( '[name="key"]' ).val() == '' ) {
            swal( {
                icon: 'error',
                text: 'Przeciagnij suwak w prawo by potwierdzić, że nie jesteś robotem a następnie kliknij przycisk wyślij!',
                buttons: {
                    text: 'Zamknij',
                },
            } );
            return false;
        }

        if (isPrivacyPolicyChecked() === false) {
            swal( {
                icon: 'warning',
                title: 'Brak akceptacji',
                text: 'Aby wysłać wiadomość należy zaakceptować politykę prywatności',
                buttons: {
                    text: 'Rozumiem',
                },
            } );

            return false;
        }

        showContactFormProcessingSpinner();

        let mail_data = $('#contactForm').serializeArray();

        $.ajax( {
            url: window.global_data.send_question_form_url,
            data: {
                mail_data: mail_data,
            },
            dataType: 'json',
            type: 'POST',
            success: function ( data ) {
                hideContactFormProcessingSpinner();

                $('.loader').fadeOut();
                if (data.result == 'success') {
                    swal( {
                        icon: 'success',
                        text: data.message,
                        buttons: {
                            text: 'Zamknij',
                        },
                    } );

                    $('#contactForm').find('input, textarea').val('');
                    $('.anim-typewriter-hide' ).removeClass('anim-typewriter-hide');
                    $('.anim-typewriter').removeClass('anim-typewriter');
                    $('#privacy-policy').prop('checked', false);

                    initalizeSlider( true );
                } else {
                    swal( {
                        icon: 'error',
                        text: data.message,
                        buttons: {
                            text: 'Zamknij',
                        },
                    });
                }
            },
        } );
    });
}

const isPrivacyPolicyChecked = () => $( '#privacy-policy' ).prop( 'checked' ) === true;

function initalizeSlider( re = false ) {
    if (re) $('#slider').empty();

    new SlideToUnlock( $( '#slider' ), {
        useData: true,
        theme: 'grey',
        unlockfn: function () {
            if (window.isFormValid === false) {
                initalizeSlider(true);

                swal( {
                    icon: 'warning',
                    title: 'Formularz nie jest wypełniony',
                    text: 'Aby wysłać wiadomość należy wypełnić formularz',
                    buttons: {
                        text: 'Rozumiem',
                    },
                });
            } else if (isPrivacyPolicyChecked() === false){
                initalizeSlider(true);

                swal( {
                    icon: 'warning',
                    title: 'Brak akceptacji',
                    text: 'Aby wysłać wiadomość należy zaakceptować politykę prywatności',
                    buttons: {
                        text: 'Rozumiem',
                    },
                } );
            } else {
                if (window.isFormValid === true) {
                    $( '#sendContactForm' ).attr( 'disabled', false );
                    $( '[name="' + $( '#slider' ).data( 'input' ) + '"]' ).val( $( '#slider' ).data( 'key' ) );
                }
            } 
        },
        lockfn: function () {
            $( '#sendContactForm' ).attr( 'disabled', true );
            $( '[name="' + $( '#slider' ).data( 'input' ) + '"]' ).val( '' );
        },
    } );
}