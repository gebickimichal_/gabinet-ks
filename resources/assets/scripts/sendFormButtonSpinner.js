export let buttonText = undefined;

export const showContactFormProcessingSpinner = () => {
    buttonText = $('#sendContactForm').html();
    $('#sendContactForm').html('');
    $('#sendContactForm').attr('uk-spinner', 'uk-spinner');
}

export const hideContactFormProcessingSpinner = () => {
    $('#sendContactForm').html(buttonText);
    $('#sendContactForm').removeAttr('uk-spinner');
}

