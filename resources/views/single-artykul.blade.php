@extends('layouts.app')

@section('title', $pageTitle)

@section('facebookTitle', $article->title)
@section('facebookImageUrl', $article->imageUrl)

@section('content')

@include('partials.header-title')

<main>
    <section class="sectiom-single-post">

        <div class="uk-container uk-container-large">
          <div class="blog-grid" data-uk-grid>

                <div class="uk-width-1-1 uk-width-3-5@s uk-width-2-3@m uk-width-3-4@lg uk-width-3-4@xl">

                    <div data-uk-grid>

                        <div class="uk-width-1-1@s uk-width-1-1@m uk-width-1-1@l uk-width-5-6@xl">

                            <div class="uk-grid-collapse uk-margin">

                                <div class="revert-blog-card" data-uk-grid>

                                    <div
                                        class="uk-width-1-1@s uk-width-1-2@m uk-width-1-1@l uk-width-1-1@lg uk-card-media-left center-flex">
                                        <img class="no-mar" src="{!! $article->imageUrl !!}" alt="Zdjęcie w nagłówku artykułu">

                                    </div>

                                    <div
                                        class="uk-width-1-1@s uk-width-1-2@m uk-width-1-1@l uk-width-1-1@lg uk-card-media-left uk-padding-remove-left uk-margin-remove-top">

                                        <div class="uk-card-body">
                                            <p class="post-details text-semibold">
                                                <span>{{ date('j.n.Y', strtotime($article->post_date)) }}</span>

                                                @foreach ($article->tags as $tag)
                                                    <a href="{!! $tag->url !!}" class="post-details__tag">{{ $tag->name }}</a>
                                                @endforeach
                                            </p>
                                            
                                            @if($article->title)
                                                <h3 class="h3">{{ $article->title }}</h3>
                                            @endif

                                            <div class="edit-text-box">

                                                {!! apply_filters('the_content', $article->content) !!}

                                            </div>

                                            @if ($article->bibliography)
                                            <div class="bibliography">

                                                <div class="edit-text-box">

                                                    <h3>Bibliografia</h3>

                                                    {!! apply_filters('the_content', $article->bibliography) !!}
                                                </div>

                                            </div>
                                            @endif

                                        </div>

                                        <div class="uk-card-footer card-footer">

                                            <a href="https://www.facebook.com/sharer/sharer.php?u={{ get_permalink($article) }}"
                                                target="_blank" data-uk-tooltip="Udostępnij na Facebooku" title=""
                                                aria-expanded="false" class="jtb-social-link">

                                                udostępnij

                                                <svg xmlns="http://www.w3.org/2000/svg" width="8.067" height="17"
                                                    viewBox="416.912 170 8.067 17">
                                                    <path fill="#333"
                                                        d="M424.979 175.897h-2.956v-2.158c0-.662.684-.815 1.004-.815h1.911v-2.913l-2.19-.011c-2.987 0-3.669 2.172-3.669 3.563v2.334h-2.167v3.001h2.167V187h2.944v-8.102h2.5l.456-3.001z">
                                                    </path>
                                                </svg>
                                            </a>

                                        </div>

                                    </div>

                                </div>


                            </div>


                        </div>


                    </div>

                </div>

                <aside class="aside uk-width-1-1 uk-width-2-5@s uk-width-1-3@m uk-width-1-4@lg uk-width-1-4@xl">

                    <h2 class="uk-hidden">Wyszukiwarka, kategorie i tagi</h2>

                    @include('partials.search-form')

                    @include('partials.articles-categories')

                    @include('partials.articles-tags')

                    @include('partials.sidelists')

                </aside>
            </div>
        </div>

        {{ comments_template() }}
    </section>
</main>

@endsection
