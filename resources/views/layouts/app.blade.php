<!doctype html>
<html {!! get_language_attributes() !!}>

@include('partials.head')

<body @php(body_class())>

    <div class="overlay" id="overlay">
      <img data-src="@asset('images/svg/sk-logo.svg')" data-uk-img alt="Logo" class="uk-preserve">
    </div>

    <h1 class="uk-hidden">Smosarska</h1>

    <div class="fixed-div-height"></div>

    @include('partials.header')

    @yield('content')

    <a href="#0" class="cd-top js-cd-top"></a>

    @include('partials.navigations.footer-navigation')

    @php(wp_footer())

    @include('partials.navigations.offside-navigation')
</body>

</html>
