@extends('layouts.app')

@section('title', $pageTitle)

@section('content')

    @include('partials.header-title')

    <main>
        <section>

            <div class="uk-container uk-container-large">
                <div data-uk-grid class="uk-grid-large">

                    <div class="uk-width-1-1@s uk-width-1-2@m uk-width-1-2@lg uk-width-1-2@xl">

                        <div class="image-effect-box">
                            <img data-src="{{ $firstImage }}" data-uk-img alt="Zdjęcie Pani Smosarskiej"
                                class="uk-preserve image-effect-box--img">
                        </div>

                    </div>

                    <div class="uk-width-1-1@s uk-width-1-2@m uk-width-1-2@lg uk-width-1-2@xl">

                        <h2 class="section-title text-bold letter-spaceing-50 h2">{{ $name }}</h2>

                        <p class="banner-text-div__first-line text-light letter-spaceing-200 text-green mar-bottom-50">
                            DIETETYK KLINICZNY</p>

                        <div class="edit-text-box">

                            {!! apply_filters('the_content', $subNameDescription) !!}

                        </div>

                    </div>

                </div>
            </div>
        </section>

        <section>

            <div class="uk-container uk-container-large">
                <div data-uk-grid class="uk-grid-large">

                    <div class="uk-width-1-1@s uk-width-1-2@m uk-width-1-2@lg uk-width-1-2@xl">


                        <div class="bibliography">

                            <div class="edit-text-box">

                                <h3>Kwalifikacje</h3>

                                {!! apply_filters('the_content', $qualifications) !!}

                                <h3>Doswiadczenie</h3>

                                {!! apply_filters('the_content', $experience) !!}

                            </div>

                        </div>

                    </div>

                    <div class="uk-width-1-1@s uk-width-1-2@m uk-width-1-2@lg uk-width-1-2@xl">

                        <img data-src="{{ $secondImage }}" data-uk-img alt="Miska ze zdrową żywnością"
                            class="uk-preserve image-effect-box--img extra-top-mar">

                    </div>

                </div>
            </div>

        </section>

        <section>

            <div class="uk-container uk-container-large">

                <h3 class="h3 uk-margin-medium-bottom">Certyfikaty</h3>


                <div class="dot-slide" data-uk-slider>

                    <div class="uk-position-relative uk-visible-toggle" tabindex="-1">

                        <ul class="uk-slider-items uk-child-width-1-1 uk-child-width-1-2@s uk-child-width-1-3@m uk-child-width-1-4@l uk-grid"
                            data-uk-lightbox="animation: slide">


                            @foreach ($certificatesList as $certificateId)
                            <li>
                                <a class="uk-inline" href="{{wp_get_attachment_url($certificateId) }}">
                                    <div class="uk-panel">
                                        <img src="{{ wp_get_attachment_url($certificateId) }}" alt="">
                                        <div class="plus-div">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="101" height="101">
                                                <path fill-rule="evenodd" clip-rule="evenodd" fill="#f1f1f1"
                                                    d="M101 49.826H51.173V0h-1.346v49.826H0v1.347h49.827V101h1.346V51.173H101z" />
                                            </svg>
                                        </div>
                                    </div>

                                </a>
                            </li>
                            @endforeach

                        </ul>

                    </div>

                    <ul class="uk-slider-nav uk-dotnav uk-flex-center"></ul>

                </div>

            </div>

            @include('partials.ask-question')

        </section>
    </main>

@endsection
