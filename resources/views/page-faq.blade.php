@extends('layouts.app')

@section('title', $pageTitle)

@section('content')

@include('partials.header-title')

<main>
    <section class="faq">
        <div class="uk-container uk-container-small">
            <div data-uk-grid>
                <div class="uk-width-1-1@s uk-width-1-1@m uk-width-1-1@l uk-width-5-6@xl margin-center">
                <ul data-uk-accordion>
                    @foreach($questionsAndAnswers as $questionAndAnswer)
                    <li>
                        <a class="uk-accordion-title" href="#">
                            <div class="uk-accordion-title__container">
                                {{ $questionAndAnswer['question'] }}
                            </div>
                        </a>
                        <div class="uk-accordion-content">
                            <div class="edit-text-box">
                                {!! apply_filters('the_content', $questionAndAnswer['answer']) !!}
                            </div>
                        </div>
                    </li>
                    @endforeach
                </ul>
                </div>
            </div>
        </div>

        @include('partials.ask-question')
    </section>
</main>

@endsection
