@extends('layouts.app')

@section('title', $pageTitle)

@section('content')

    @include('partials.header-title')

    <main>
        <section>
            <div class="uk-container uk-container-large">
                <div data-uk-grid>

                    <div class="uk-width-1-1 uk-width-3-5@s uk-width-2-3@m uk-width-3-4@lg uk-width-3-4@xl">
                        <div data-uk-grid>

                            @include('partials.articles')

                        </div>

                    </div>

                    <aside class="aside uk-width-1-1 uk-width-2-5@s uk-width-1-3@m uk-width-1-4@lg uk-width-1-4@xl">

                        <h2 class="uk-hidden">Wyszukiwarka, kategorie i tagi</h2>

                        @include('partials.search-form')

                        @include('partials.articles-categories')

                        @include('partials.articles-tags')

                        @include ('partials.sidelists')

                    </aside>

                </div>
            </div>
        </section>
    </main>
@endsection