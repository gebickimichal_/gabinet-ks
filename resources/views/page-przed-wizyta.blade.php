@extends('layouts.app')

@section('title', $pageTitle)

@section('content')
    @include('partials.header-title')
    <main>
        <section>
            <div class="uk-container uk-container-large">
                <div class="uk-grid-large" data-uk-grid>

                    <div class="uk-width-1-1@s uk-width-1-2@m uk-width-1-2@lg uk-width-1-2@xl">

                        <div class="image-effect-box">
                            <img data-src="{{ $preAppointmentDescriptionImage }}" data-uk-img alt="Kobieta wypełniająca dziennik żywienia"
                                class="uk-preserve image-effect-box--img">
                        </div>

                    </div>

                    <div class="uk-width-1-1@s uk-width-1-2@m uk-width-1-2@lg uk-width-1-2@xl">

                        <h2 class="section-title text-bold letter-spaceing-50 h2 special-margin">{{ $preAppointmentDescriptionHeading }}</h2>

                        <p class="banner-text-div__first-line text-light letter-spaceing-200 text-green mar-bottom-50">
                            gabinet dietetyki klinicznej</p>

                        <div class="edit-text-box">
                            {!! apply_filters('the_content', $preAppointmentDescriptionText) !!}
                        </div>

                    </div>

                </div>
            </div>

            <div class="uk-container uk-container-large download-section">

                <div class="uk-grid-large" data-uk-grid>

                    <div class="uk-width-1-1@s uk-width-1-2@m uk-width-1-2@lg uk-width-1-2@xl">
                        <ol data-uk-accordion>

                            @foreach ($preAppointmentActionsList as $preAppointmentAction)
                            <li class="uk-accordion-list">
                                <a class="uk-accordion-title" href="#">

                                    <div class="uk-accordion-title__container">
                                        {{ $preAppointmentAction['action_title'] }}
                                    </div>

                                </a>

                                <div class="uk-accordion-content">


                                    <div class="edit-text-box">

                                        {!! apply_filters('the_content', $preAppointmentAction['action_text']) !!}

                                    </div>

                                </div>
                            </li>
                            @endforeach

                        </ol>
                    </div>

                    <div class="uk-width-1-1@s uk-width-1-2@m uk-width-1-2@lg uk-width-1-2@xl">

                        <p class="download-section__title">
                            DO POBRANIA
                        </p>

                        <ul class="download-section__container">
                            @foreach ($preAppointmentDownloadsList as $preAppointmentDownloadFile)
                            <li class="download-block">

                                <p class="download-block__para">
                                    {{ $preAppointmentDownloadFile['file_name'] }}
                                </p>
                                <a  class="button button--green button--size button--transparent button--text text-semibold"
                                    href="{{ $preAppointmentDownloadFile['file_download_url'] }}"
                                    target="_blank">

                                    POBIERZ
                                </a>
                            </li>

                            <li class="download-block download-block-divider">

                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>

            @include('partials.ask-question')
        </section>
    </main>
@endsection
