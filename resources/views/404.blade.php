<!doctype html>
<html {!! get_language_attributes() !!}>

@include('partials.head')

@section('title', '404 - Gabinet Dietetyki Klinicznej')

<body class="error404">
	<section class="er-404-section">

		<div class="uk-width-1-1 er-bg er-404 p-0">
			<div class="uk-container uk-container-large">

				<div data-uk-grid class="uk-grid uk-grid-large er-100-div">

					<div class="uk-width-1-1@s uk-width-1-2@m uk-width-1-2@lg uk-width-1-2@xl no-padding uk-first-column">
							<img data-src="@asset('images/404.jpg')" data-uk-img="" alt="Zdjęcie na stronie 404"
								class="uk-preserve image-effect-box--img"
								src="@asset('images/404.jpg')">
					</div>
					<div class="uk-width-1-1 uk-width-1-1@s uk-width-1-2@m uk-width-1-2@l uk-width-1-2@xl er-center-it">

							<div class="er-center-it uk-padding-remove">

								<p class="er-error-title">Błąd <span>404</span></p>

								<p>Strona, której szukasz nie istnieje.</p>
							</div>
			  <a 	class="button button--green button--size-big button--empty button--text text-semibold"
			  		href="{!! get_home_url() !!}">

                  STRONA GŁÓWNA

              </a>
					</div>

				</div>

			</div>
		</div>
  </section>
  @php(wp_footer())
</body>
</html>