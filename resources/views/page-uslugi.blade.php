@extends('layouts.app')

@section('title', $pageTitle)

@section('content')
    @include('partials.header-title')
    <main>
        <section>
        <div class="uk-container uk-container-large">
                <div data-uk-grid>
                    <div class="uk-width-1-1@s uk-width-1-1@m uk-width-1-1@l uk-width-5-6@xl margin-center">
                        <ul data-uk-accordion>
                            @foreach ($serviceCategories as $serviceCategory)
                            <li>
                                <a class="uk-accordion-title" href="#">
                                    <div class="uk-accordion-title__container">
                                        {{ $serviceCategory['service_category_name'] }}
                                    </div>
                                </a>
                                <div class="uk-accordion-content">
                                    <div class="uk-child-width-1-1 uk-child-width-1-2@s uk-child-width-1-3@m uk-child-width-1-4@l uk-child-width-1-4@xl uk-grid-small uk-grid-match uk-grid-medium" data-uk-grid>
                                        @foreach ($serviceCategory['service_card'] as $serviceCard)
                                        <div>
                                            <div class="uk-card">
                                                <div class="card-head">
                                                    <h3 class="uk-card-title">{{ $serviceCard['service_name'] }}</h3>
                                                </div>
                                                <div class="card-body">
                                                    <p class="card-body__price">
                                                        <span>
                                                            {{ $serviceCard['service_price'] }}
                                                        </span>
                                                        @if ($serviceCard['service_duration'])
                                                            | 55 min.
                                                        @endif
                                                    </p>
                                                    @if ($serviceCard['service_wait_time'])
                                                        <span class="card-body__span">
                                                            czas oczekiwania {{ $serviceCard['service_wait_time'] }}
                                                        </span>
                                                    @endif
                                                </div>
                                                <div class="card-footer">
                                                    {!! apply_filters('the_content', $serviceCard['service_description_list']) !!}
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>

            @include('partials.ask-question')
        </section>
    </main>
@endsection
