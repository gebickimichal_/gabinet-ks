@extends('layouts.app')

@section('title', $pageTitle)

@section('content')

    @include('partials.header-title')

    <main>
        <section>

          <div class="uk-container uk-container-large uk-margin-large-bottom">

            <div class="edit-text-box">
              {!! apply_filters('the_content', $content) !!}
            </div>


          </div>
        </section>
    </main>

@endsection
