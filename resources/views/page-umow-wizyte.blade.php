@extends('layouts.app')

@section('title', $pageTitle)

@section('content')

@include('partials.header-title')

<main>
    <section>

        <div class="uk-container uk-container-large mar-bottom-100">
            <div class="uk-grid-large" data-uk-grid data-uk-scrollspy="target: > div; cls: uk-animation-fade; delay: 350">
    
                <div class="uk-width-1-1 uk-width-1-2@s uk-width-1-3@m uk-width-1-3@lg uk-width-1-3@xl">
    
                    <div class="uk-card card-visit">
    
    
                        <img data-src="@asset('images/svg/known-doctor.svg')" data-uk-img alt="Logo znany lekarz" class="uk-preserve">   
    
                        <h3 class="uk-card-title card-visit__title">Umów się przez portal znany lekarz</h3>
    
                        <div class="content">
                            <iframe src="{{get_site_url()}}/wp-admin/admin-ajax.php?action=get_widget" style="overflow:hidden;border:0px;">
                            </iframe>
                        </div>    
    
                    </div>
    
                </div>
    
                <div class="uk-width-1-1 uk-width-1-2@s uk-width-1-3@m uk-width-1-3@lg uk-width-1-3@xl">

                    <div class="uk-card card-visit">

                        <img data-src="@asset('images/svg/telephone.svg')" data-uk-img alt="Logo" class="uk-preserve">   

                        <h3 class="uk-card-title card-visit__title">Umów się przez telefon</h3>

                        <p class="card-visit-subtitle">

                            <a class="card-visit-subtitle__link" href="tel:{{ $telephoneNumber }}">
                                {{ $telephoneNumber }}
                            </a>

                        </p>

                    </div>
    
                </div>
      
                <div class="uk-width-1-1 uk-width-1-2@s uk-width-1-3@m uk-width-1-3@lg uk-width-1-3@xl">
    
                    <div class="uk-card card-visit">

                        <img data-src="@asset('images/svg/mail.svg')" data-uk-img alt="" class="uk-preserve">   

                        <h3 class="uk-card-title card-visit__title">Umów się przez e-mail</h3>

                        <p class="card-visit-subtitle">

                            <a class="card-visit-subtitle__link" href="mailto:{{ $emailAddress }}">
                                {{ $emailAddress }}
                            </a>

                        </p>

                    </div>

                </div>
    
            </div>
        </div>
    </section>
</main>
@endsection