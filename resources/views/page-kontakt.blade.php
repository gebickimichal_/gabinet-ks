@extends('layouts.app')

@section('title', $pageTitle)

@section('content')
    <main>

        <section class="contact">

            <div class="uk-container uk-container-large">
                <div class="uk-grid-large" data-uk-grid>

                    <div class="uk-width-1-1@s uk-width-1-2@m uk-width-1-2@lg uk-width-1-2@xl">

                        <ul class="download-section__container contact-container">

                            <li>
                                <h2 class="section-title text-bold letter-spaceing-50 h2 margin-top">Kontakt</h2>                            
                            </li>

                            <li class="download-block">

                                <p class="banner-text-div__first-line text-light letter-spaceing-200 text-green mar-bottom-30">
                                ADRES</p>

                                <p class="card-visit-subtitle">
                                    <a class="card-visit-subtitle__link">
                                        {{ $address }}
                                    </a>
                                </p>

                            </li>


                            <li class="download-block">

                                <p
                                class="banner-text-div__first-line text-light letter-spaceing-200 text-green mar-bottom-30">
                                TELEFON</p>

                                <p class="card-visit-subtitle">
                                    <a class="card-visit-subtitle__link" href="tel:{{ $telephoneNumber }}">
                                        {{ $telephoneNumber }}
                                    </a>
                                </p>

                            </li>

                            <li class="download-block">

                                <p
                                    class="banner-text-div__first-line text-light letter-spaceing-200 text-green mar-bottom-30">
                                    EMAIL</p>
                                    <p class="card-visit-subtitle">
                                    <a class="card-visit-subtitle__link" href="mailto:{{ $email }}">
                                        {{ $email }}
                                    </a>
                                </p>

                            </li>

                        </ul>

                    </div>

                    <div class="uk-width-1-1@s uk-width-1-2@m uk-width-1-2@lg uk-width-1-2@xl">

                        <div class="form-container">

                            <h2 class="section-title text-bold letter-spaceing-50 h2">Napisz do mnie</h2>

                            <form id="contactForm" class="uk-grid-small" data-uk-grid>

                                <div class="uk-width-1-1 uk-width-1-2@s uk-width-1-2@m uk-width-1-2@l uk-width-1-2@xl uk-position-relative">

                                    <input class="uk-input require-this" type="text" placeholder="imie i nazwisko" data-r_min="3" data-r_max="4" data-r_text="Pole wymagane" name="name" data-validity="BAD">

                                    <span class="val-text">Pole wymagane</span>

                                </div>
                                <div class="uk-width-1-1 uk-width-1-2@s uk-width-1-2@m uk-width-1-2@l uk-width-1-2@xl uk-position-relative">

                                    <input class="uk-input require-this" type="email" placeholder="e-mail" data-r_min="3" data-r_max="4" data-r_text="Pole wymagane" name="email" data-validity="BAD">
                                    <span class="val-text"></span>

                                </div>
                                <div class="uk-width-1-1 uk-width-1-2@s uk-width-1-2@m uk-width-1-2@l uk-width-1-2@xl uk-position-relative">

                                    <input class="uk-input require-this" type="text" placeholder="telefon" data-r_min="7" data-r_max="9" data-r_text="Pole wymagane" name="telephone" data-validity="BAD">
                                    <span class="val-text">Pole wymagane</span>

                                </div>
                                <div class="uk-width-1-1 uk-width-1-2@s uk-width-1-2@m uk-width-1-2@l uk-width-1-2@xl uk-position-relative">

                                    <input class="uk-input require-this" type="text" placeholder="temat" data-r_min="3" data-r_max="4" data-r_text="Pole wymagane" name="subject" data-validity="BAD">
                                    <span class="val-text">Pole wymagane</span>

                                </div>
                                <div class="uk-width-1-1@s uk-position-relative">

                                    <textarea class="uk-textarea require-this" rows="12" placeholder="wiadomość" data-r_min="3" data-r_max="4" data-r_text="Pole wymagane" name="message" data-validity="BAD"></textarea>
                                    <span class="val-text">Pole wymagane</span>

                                </div>

                                <div class="uk-width-1-1@s">
                                    <div class="uk-margin uk-grid-small uk-child-width-auto uk-grid">
                                        <label class="label"><input id="privacy-policy" class="uk-checkbox" type="checkbox">

                                        <span style="font-size:1.3rem">
                                            Zgadzam się na przechowywanie danych osobowych i przetwarzanie ich przez Gabinet Dietetyki Klinicznej w celu kontaktu zwrotnego oraz wysłania skierowanej do mnie oferty handlowej za pomocą środków komunikacji elektronicznej <a class="regulations" href="/polityka-prywatnosci">więcej</a>
                                        </span>

                                        </label>

                                    </div>
                                </div>

                                <div class="uk-width-1-1@s">

                                <div id="slider" data-lock-text="Przeciągnij w prawo by odblokować"
                                    data-unlock-text="A teraz wyślij !"
                                    data-input="key" class="slideToUnlock locked">
                                </div>

                                </div>
                                <div class="uk-width-1-1@s">
                                    <button class="button button--green button--size
                                    button--transparent button--text text-semibold" id="sendContactForm" disabled>
                                        WYŚLIJ
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </section>

        <div class="map_container">

                <div id="map"></div>

        </div>
    </main>
@endsection
