@extends('layouts.app')

@section('title', $pageTitle)

@section('content')

    @include('partials.mainPage.banner')

    @include('partials.mainPage.opis')

    @include('partials.mainPage.recipe')

    @include('partials.mainPage.blog-section')

@endsection