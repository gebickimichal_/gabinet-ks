<div class="header-title-box">
    <img data-src="{{ $headerImage }}" data-uk-img alt="Zdjęcie w nagłówku strony" class="uk-preserve header-title-box__img">

    <div class="header-title">
        <h2 class="h2 text-bold">{{ $headerTitle }}</h2>
    </div>

</div>