<form role="search" method="get" id="searchform" class="search-form" action="{{ $searchUrl }}">
    <div class="input-group"> <label class="screen-reader-text"></label>
        <input type="text" name="search" value="{{ stripslashes($searchQuery) }}" placeholder="szukaj..."
            id="search-form-5ddf74dbd7cf7" class="form-control form-search-control" required="">
        <button type="submit" class="search-btn">
            <svg xmlns="http://www.w3.org/2000/svg" width="17.948" height="16.093">
                <g fill="#408E42">
                    <path
                        d="M.328 14.222a1.08 1.08 0 000 1.551 1.14 1.14 0 001.586 0l2.844-2.781a8.206 8.206 0 01-1.326-1.804L.328 14.222zM10.648 0h-.012l-.031.001v.001a7.298 7.298 0 000 14.594v.001l.021.001h.024A7.3 7.3 0 0017.949 7.3 7.303 7.303 0 0010.648 0zm5.559 7.3a5.312 5.312 0 01-1.625 3.815 5.626 5.626 0 01-3.933 1.588 5.613 5.613 0 01-3.956-1.613A5.418 5.418 0 015.57 9.486 5.242 5.242 0 015.09 7.3a5.31 5.31 0 011.625-3.815c1.008-.981 2.396-1.59 3.934-1.59s2.926.608 3.932 1.59 1.626 2.328 1.626 3.815z" />
                </g>
            </svg>
        </button>
    </div>
</form>