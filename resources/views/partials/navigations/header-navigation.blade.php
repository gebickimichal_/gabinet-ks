<nav class="uk-navbar-container uk-container-expand uk-navbar-transparent hide-header__primary-nav" data-uk-navbar>
    <h2 class="uk-hidden">Header navigation</h2>

    <div class="uk-navbar-left">
        <a href="{{ substr(get_site_url(), 0, strrpos(get_site_url(), '/')) }}" class="uk-logo" title="Logo">
            <img data-src="@asset('images/svg/sk-logo.svg')" data-uk-img alt="Logo strony" class="uk-preserve">
        </a>
    </div>

    <div class="uk-navbar-right">
        <ul class="uk-navbar-nav header-nav hide-desktop">
            @foreach ($navigationElements as $navigationElement)

                @if ($navigationElement->label !== 'UMÓW WIZYTĘ')
                    @if (count($navigationElement->children) === 0)
                        <li class="header-nav__list">
                            <a class="header-nav__link text-semibold {{ $navigationElement->active ? 'active-menu-link' : '' }}" href="{!! $navigationElement->url !!}">{{ $navigationElement->label }}</a>
                        </li>
                    @else
                        <li class="header-nav__list parent-li">
                            <a class="header-nav__link text-semibold {{ $navigationElement->active || $navigationElement->activeAncestor ? 'active-menu-link' : '' }}">{{ $navigationElement->label }}</a>
            
                            <div class="uk-navbar-dropdown">
            
                                <ul class="uk-nav uk-navbar-dropdown-nav">
                                    @foreach ($navigationElement->children as $child)
                                    <li class="uk-active">
                                        <a href="{!! $child->url !!}" class="{{ $child->active ? 'active-menu-link' : '' }}">{{ $child->label }}</a>
                                    </li>
                                    @endforeach
                                </ul>
            
                            </div>
            
                        </li>
                    @endif
                @endif
            @endforeach
        </ul>
    </div>

    <a class="uk-navbar-toggle hide-desktop-nav padding-right-10" data-uk-navbar-toggle-icon
    data-uk-toggle="target: #offcanvas-push"></a>

    <a class="button button--green button--size button--text text-semibold hide-mobile-639"
        href="{{ $navigationElement->url }}">
        UMÓW WIZYTĘ
    </a>

</nav>