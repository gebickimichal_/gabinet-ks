<div id="offcanvas-push" data-uk-offcanvas="mode: push; overlay: true">
    <div class="uk-offcanvas-bar">

        <button class="uk-offcanvas-close" type="button" data-uk-close></button>

        <div class="uk-nav uk-nav-default">
            <ul class="uk-navbar-nav" data-uk-navbar="mode: click">
                @foreach ($navigationElements as $navigationElement)
                    @if (count($navigationElement->children) > 0)
                    <li class="parent-li">
                        <a class="">{{ $navigationElement->label }}</a>
                        <div class="uk-navbar-dropdown">
                            <ul class="uk-nav uk-navbar-dropdown-nav">
                                @foreach ($navigationElement->children as $navigationElementChild)
                                    <li class="uk-active"><a href="{!! $navigationElementChild->url !!}">{{ $navigationElementChild->label }}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </li>
                    @else
                    <li class="">
                        <a class="" href="{!! $navigationElement->url !!}">{{ $navigationElement->label }}</a>
                    </li>
                    @endif
                @endforeach
            </ul>
        </div>

        <a class="button button--green button--size-full button--empty button--text text-semibold"
            href="{{ $navigationElement->url }}">
            UMÓW WIZYTĘ
        </a>

        <ul class="social-media-list">

            <li class="social-media-list__item text-light letter-spaceing-100">
                {{ $address }}
            </li>

            <li class="social-media-list__item text-light letter-spaceing-100">
                    <a class="social-media-link" href="tel:{{ $telephoneNumber }}">tel. {{ $telephoneNumber }}</a>
            </li>


        </ul>

    </div>
</div>