<div id="modal-full" class="uk-modal-full" data-uk-modal="bg-close: false">

    <div class="uk-modal-dialog">

        <button class="uk-modal-close-full uk-close-large" type="button" data-uk-close></button>

        <div class="uk-container">
            <div class="uk-grid-collapse uk-child-width-1-1 uk-child-width-1-1@s uk-child-width-1-2@m uk-flex-middle" data-uk-grid>

                <div class="hide-tablet">
                    <img data-src="@asset('images/before-a-visit.jpg')" data-uk-img alt="Kobieta wypełniająca kalendarz żywienia"
                        class="uk-preserve image-effect-box--img">
                </div>

                <div class="uk-padding-large">
                    <div class="">

                        <h2 class="section-title text-bold letter-spaceing-50 h2">Zadaj pytanie</h2>

                        <form id="contactForm" class="uk-grid-small" data-uk-grid>

                            <div class=" uk-width-1-1">
                                <select class="uk-select" name="client_type">
                                    <option value="nowy klient">jestem nowym klientem</option>
                                    <option value="powracający klient">jestem powracającym klientem</option>
                                </select>
                            </div>

                            <div class="uk-width-1-1 uk-position-relative">

                                <input class="uk-input require-this" type="text" placeholder="imie i nazwisko"
                                data-r_min="3" data-r_max="4" data-r_text="Pole wymagane" name="name"
                                data-validity="BAD">

                                <span class="val-text">Pole wymagane</span>

                            </div>
                            <div class="uk-width-1-1 uk-position-relative">

                                <input class="uk-input require-this" type="email" placeholder="e-mail" data-r_min="3"
                                data-r_max="4" data-r_text="Pole wymagane" name="email" data-validity="BAD">
                                <span class="val-text"></span>

                            </div>
                            <div class="uk-width-1-1 uk-position-relative">

                                <input class="uk-input require-this" type="text" placeholder="telefon" data-r_min="7"
                                data-r_max="9" data-r_text="Pole wymagane" name="telephone" data-validity="BAD">
                                <span class="val-text">Pole wymagane</span>

                            </div>

                            <div class="uk-width-1-1@s uk-position-relative">
                                <textarea class="uk-textarea require-this" rows="12" placeholder="wiadomość"
                                    data-r_min="3" data-r_max="4" data-r_text="Pole wymagane" name="message"
                                    data-validity="BAD">
                                </textarea>

                                <span class="val-text">Pole wymagane</span>
                            </div>

                            <div class="uk-width-1-1@s">
                                <div class="uk-margin uk-grid-small uk-child-width-auto uk-grid">
                                    <label class="label">
                                        <input id="privacy-policy" class="uk-checkbox" type="checkbox">

                                        <span style="font-size:1.3rem">
                                            Zgadzam się na przechowywanie danych osobowych i przetwarzanie ich
                                            przez Gabinet Dietetyki Klinicznej w celu kontaktu zwrotnego oraz
                                            wysłania skierowanej do mnie oferty handlowej za pomocą środków
                                            komunikacji elektronicznej <a class="regulations"
                                                href="/polityka-prywatnosci">więcej</a>
                                        </span>


                                    </label>

                                </div>
                            </div>

                            <div class="uk-width-1-1@s">

                                <div id="slider" data-lock-text="Przeciągnij w prawo by odblokować"
                                    data-unlock-text="A teraz wyślij !"
                                    data-input="key" class="slideToUnlock locked">
                                </div>

                            </div>

                            <div class="uk-width-1-1@s">

                                <button class="button button--green button--size button--transparent button--text text-semibold"
                                    id="sendContactForm" disabled>

                                    WYŚLIJ

                                </button>

                            </div>

                        </form>

                    </div>
                </div>

            </div>

        </div>

    </div>

</div>
