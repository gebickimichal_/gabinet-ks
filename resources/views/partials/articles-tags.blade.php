<div class="widget widget_tag_cloud">
    <div class="container">
        <p class="banner-text-div__first-line text-light letter-spaceing-200 text-green mar-bottom-50">
            TAGI
        </p>
    </div>

    <div class="tagcloud">
        @foreach ($articlesTags as $tag)
            <a  href="{{ $tag->url }}"
                class="tag-cloud-link tag-link-44 tag-link-position-45"
                aria-label="Znaczenie barw (2 elementy)">
                {{ mb_strtoupper($tag->name) }}
            </a>
        @endforeach
    </div>
</div>