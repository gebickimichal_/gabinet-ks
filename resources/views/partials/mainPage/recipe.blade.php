<section>
    <h2 class="uk-invisible">Opis</h2>

    <div class="uk-container uk-container-xsmall uk-container-expand-right uk-padding-remove-right section-bg">

        <div data-uk-grid>

            <div
                class="uk-width-1-1@s uk-width-1-1@m uk-width-1-1@lg uk-width-1-1@xl uk-position-relative uk-padding-remove-left">

                <div class="uk-container uk-container-expand white-bg box-shadow recipe-for-success-box">

                    <div data-uk-grid>

                        <div class="uk-width-1-1@s uk-width-1-2@m uk-width-1-2@lg uk-width-1-2@xl center-flex">

                            @if($recipeTitle)
                                <h2 class="section-title section-title--small text-bold letter-spaceing-50 h2">{{ $recipeTitle }}</h2>
                            @endif

                        </div>

                        <div class="uk-width-1-1@s uk-width-1-2@m uk-width-1-2@lg uk-width-1-2@xl">

                            <ul class="recipe-for-success" data-uk-grid>
                                @foreach ($listItems as $listItem)
                                    <li class="uk-width-1-1 recipe-for-success__list">

                                        <img data-src="{{ $listItem['image_url'] }}" data-uk-img alt="Logo strony" class="uk-preserve">
                                        <p>
                                            {{ $listItem['title'] }}
                                        </p>

                                    </li>
                                @endforeach
                            </ul>


                        </div>

                    </div>

                </div>
            </div>

        </div>


    </div>

</section>