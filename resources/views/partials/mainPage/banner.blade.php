<div>
    <div class="uk-position-relative uk-visible-toggle main-bannner" tabindex="-1"
    data-uk-slideshow="ratio: 5:2; animation: push">
        <ul class="uk-slideshow-items">
            @foreach($slidesList as $slide)
            <li>
                <img src="{{ $slide['image_url'] }}" class="main-bannner__image" alt="" data-uk-cover>
                <div class="banner-text-div">
                    <p class="banner-text-div__first-line text-light letter-spaceing-200 text-green">GABINET
                        DIETETYKI KLINICZNEJ</p>
                    <h2 class="banner-text-div__h2 text-bold letter-spaceing-50">{{ $slide['title'] }}</h2>
                    <a href="{{ get_site_url() . '/uslugi' }}" class="button button--green button--size button--empty button--text text-semibold">
                        OFERTA
                    </a>
                </div>
            </li>
            @endforeach
        </ul>
        <ul class="uk-slideshow-nav uk-dotnav uk-margin"></ul>
    </div>
</div>