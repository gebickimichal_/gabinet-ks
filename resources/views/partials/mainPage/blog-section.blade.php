<section class="blog-section">

    <h2 class="section-title section-title--center text-bold letter-spaceing-50 h2">Ostatnio na blogu</h2>

    <div class="uk-container uk-container-large">

        <div data-uk-grid>

            @include('partials.mainPage.articles-list')

            @include('partials.mainPage.recipes-list')

        </div>

    </div>

</section>