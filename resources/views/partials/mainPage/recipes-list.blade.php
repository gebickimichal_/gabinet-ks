<div class="uk-width-1-1@s uk-width-1-2@m uk-width-1-2@lg uk-width-1-2@xl">

    <p class="banner-text-div__first-line text-light letter-spaceing-200 text-green mar-bottom-50">
        PRZEPISY</p>

    @foreach ($recipes as $recipe)
        @if ($recipe->post_status === 'publish')
        <div class="uk-grid-collapse uk-margin bottom-border">

            <div class="revert-blog-card" data-uk-grid>

                <div
                    class="uk-width-1-3@s uk-width-1-1@m uk-width-1-3@l uk-width-1-3@lg uk-card-media-left center-flex">
                    <img src="{{ $recipe->imageUrl }}" alt="Zdjęcie w tle przepisu na sukces">
                </div>

                <div
                    class="uk-width-2-3@s uk-width-1-1@m uk-width-2-3@l uk-width-2-3@lg uk-card-media-left uk-padding-remove-left">
                    <div class="uk-card-body">
                        <p class="post-details text-semibold">
                            <span>{{ date('j.n.Y', strtotime($recipe->post_date)) }}</span>

                            @foreach ($recipe->tags as $tag) 
                                <a href="{{ get_term_link($tag->term_id) }}" class="post-details__tag">{{ mb_strtoupper($tag->name) }}</a>
                            @endforeach
                        </p>

                        @if($recipe->title)
                            <h3 class="h3">{{ $recipe->title }}</h3>
                        @endif

                        <p class="text__blog">{{ $recipe->excerpt }}</p>

                        <a href="{!! $recipe->url !!}" class="read-more">czytaj więcej</a>
                    </div>
                </div>

            </div>

        </div>
        @endif
    @endforeach

    <a  class="button button--green button--size-big button--empty button--text text-semibold margin-center"
        href="{{ $recipesUrl }}">

        WSZYSTKIE PRZEPISY
    </a>

</div>