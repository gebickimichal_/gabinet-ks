<section>
    <h2 class="uk-invisible">Opis</h2>

    <div class="uk-container uk-container-large">
        <div class="uk-grid-large" data-uk-grid>

            <div class="uk-width-1-1@s uk-width-1-2@m uk-width-1-2@lg uk-width-1-2@xl">

                <div class="image-effect-box">
                    <img data-src="{{ $descriptionImageUrl }}" data-uk-img alt="Zdjęcie Pani Smosarskiej"
                        class="uk-preserve image-effect-box--img">
                </div>

            </div>

            <div class="uk-width-1-1@s uk-width-1-2@m uk-width-1-2@lg uk-width-1-2@xl">

                @if($name)
                    <h2 class="section-title text-bold letter-spaceing-50 h2 special-margin">{{ $name }}</h2>
                @endif
                
                <p class="banner-text-div__first-line text-light letter-spaceing-200 text-green mar-bottom-50">Dietetyk Kliniczny</p>

                <div class="edit-text-box">

                    {!! apply_filters('the_content', $subNameDescription) !!}

                </div>

                <a class="button button--green button--size button--empty button--text text-semibold margin-top-42"
                    href="{{ home_url('/o-mnie/') }}">

                    CZYTAJ WIĘCEJ

                </a>


            </div>

        </div>
    </div>

</section>
