<div class="widget widget_categories">
    <div class="container p-0 separator-container">
        <p
            class="banner-text-div__first-line text-light letter-spaceing-200 text-green mar-bottom-50">
            KATEGORIE</p>
    </div>
    <ul class="list-unstyled post-widget-archive-list">

        @foreach ($recipesCategories as $category)
        <li class="cat-item cat-item-27 post-widget-list-item">
            <a class="post-widget-link"
                href="{{ $category->url }}">{{ $category->name }}
                <span>{{ $category->count }}</span>
            </a>
        </li>
        @endforeach

    </ul>
</div>