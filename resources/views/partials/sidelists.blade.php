<div class="widget widget_recent_entries">
    <div class="container">

        <p class="banner-text-div__first-line text-light letter-spaceing-200 text-green mar-bottom-50">
            NAJNOWSZE ARTYKUŁY</p>
    </div>

    <ul>

        @foreach ($newArticlesList as $article)
        <li class="uk-grid-small center-flex blog-single-miniature" data-uk-grid>

            <div class="uk-width-1-1 uk-width-1-1@s uk-width-2-5@m uk-width-2-5@l uk-width-2-5@xl">
                <img src="{{ $article->imageUrl }}" alt="">
            </div>
            <a class="uk-width-1-1 uk-width-1-1@s uk-width-3-5@m uk-width-3-5@l uk-width-3-5@xl"
                href="{!! $article->url !!}">{{ $article->title }}</a>

        </li>
        @endforeach

    </ul>

</div>

<div class="widget widget_recent_entries">
    <div class="container">

        <p class="banner-text-div__first-line text-light letter-spaceing-200 text-green mar-bottom-50">
            NAJNOWSZE PRZEPISY</p>
    </div>

    <ul>

        @foreach ($newRecipesList as $recipe)
        <li class="uk-grid-small center-flex blog-single-miniature" data-uk-grid>

            <div class="uk-width-1-1 uk-width-1-1@s uk-width-2-5@m uk-width-2-5@l uk-width-2-5@xl">
                <img src="{{ $recipe->imageUrl }}" alt="">
            </div>
            <a class="uk-width-1-1 uk-width-1-1@s uk-width-3-5@m uk-width-3-5@l uk-width-3-5@xl"
                href="{!! $recipe->url !!}">{{ $recipe->title }}</a>

        </li>
        @endforeach

    </ul>

</div>