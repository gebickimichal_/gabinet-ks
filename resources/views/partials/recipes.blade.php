<div class="uk-width-1-1@s uk-width-1-1@m uk-width-1-1@l uk-width-5-6@xl">
    @if (count($recipes) > 0)
        @foreach ($recipes as $recipe)
            <div class="uk-grid-collapse uk-margin bottom-border">

                <div class="revert-blog-card" data-uk-grid>

                    <div class="uk-width-1-1@s uk-width-1-2@m uk-width-1-3@l uk-width-1-3@lg uk-card-media-left center-flex">
                        <img src="{{ $recipe->imageUrl }}" alt="">
                    </div>

                    <div class="uk-width-1-1@s uk-width-1-2@m uk-width-2-3@l uk-width-2-3@lg uk-card-media-left uk-padding-remove-left">
                        <div class="uk-card-body">
                            <p class="post-details text-semibold">
                                <span>{{ date('j.n.Y', strtotime($recipe->post_date)) }}</span>

                                @foreach ($recipe->tags as $tag)
                                    <a href="{{ $tag->url }}" class="post-details__tag">{{ mb_strtoupper($tag->name) }}</a>
                                @endforeach
                            </p>

                            @if($recipe->title)
                                <h3 class="h3">{{ $recipe->title }}</h3>
                            @endif

                            <p class="text__blog">{!! $recipe->excerpt !!}</p>

                            <a href="{!! $recipe->url !!}" class="read-more">czytaj więcej</a>
                        </div>
                    </div>

                </div>

            </div>
        @endforeach
    @else
        <p class="no-results">
          Brak wyników wyszukiwania...
        </p>
    @endif

    @include('partials.pagination')
</div>
