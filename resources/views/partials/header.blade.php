<header class="hide-header">
    <nav class="uk-navbar-container uk-container-expand uk-navbar-transparent hide-header__secondary-nav">
        <h2 class="uk-hidden">Header</h2>

        <ul class="social-media-list">

            <li class="social-media-list__item text-light letter-spaceing-100 hide-mobile-639">
                {{ $address }}
            </li>

            <li class="social-media-list__item text-light letter-spaceing-100 hide-mobile-639">
                <a class="social-media-link" href="tel:{{ $telephoneNumber }}">tel. {{ $telephoneNumber }}</a>
            </li>

            <li class="social-media-list__icons social-media-list--size">
                @if($facebookPageUrl)
                <a class="social-media-link" target="_blank" href="{{ $facebookPageUrl }}">
                    <svg class="social-media-list__svg" xmlns="http://www.w3.org/2000/svg" width="8.067" height="17"
                        viewBox="416.911 170 8.067 17">
                        <path fill="#555"
                            d="M424.979 175.896h-2.956v-2.157c0-.662.684-.815 1.004-.815h1.911v-2.913l-2.19-.011c-2.986 0-3.669 2.172-3.669 3.563v2.334h-2.167v3.002h2.167v8.102h2.944v-8.102h2.5l.456-3.003z" />
                    </svg>
                </a>
                @endif

                @if($instagramPageUrl)
                <a class="social-media-link" target="_blank" href="{{ $instagramPageUrl }}">
                    <svg class="social-media-list__svg" xmlns="http://www.w3.org/2000/svg" width="18"
                        height="17.999" viewBox="411.945 0 18 17.999">
                        <g fill="#555">
                            <path
                                d="M424.584 0h-7.275a5.368 5.368 0 00-5.363 5.362v7.275a5.368 5.368 0 005.363 5.362h7.275a5.368 5.368 0 005.361-5.362V5.362A5.368 5.368 0 00424.584 0zm3.551 12.639a3.552 3.552 0 01-3.553 3.551h-7.275a3.551 3.551 0 01-3.551-3.551V5.362a3.551 3.551 0 013.551-3.551h7.275a3.552 3.552 0 013.553 3.551v7.277z" />
                            <path
                                d="M420.945 4.345A4.661 4.661 0 00416.289 9c0 2.566 2.09 4.655 4.656 4.655S425.6 11.566 425.6 9a4.66 4.66 0 00-4.655-4.655zm0 7.5c-1.572 0-2.844-1.272-2.844-2.845a2.843 2.843 0 115.688 0 2.845 2.845 0 01-2.844 2.845z" />
                            <circle cx="425.609" cy="4.38" r="1.115" />
                        </g>
                    </svg>
                </a>
                @endif

            </li>

        </ul>

    </nav>

    @include('partials.navigations.header-navigation')

</header>
