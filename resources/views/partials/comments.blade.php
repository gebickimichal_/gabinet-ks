<div class="uk-container comment-container">

    <p class="my-partners banner-slogan-small comments-count uk-text-uppercase">
        @if (count($comments) === 0)
        <span>Brak komentarzy</span>
        @elseif (count($comments) === 1)
        <span>Komentarz (1)</span>
        @else
        <span>Komentarzy ({{ count($comments) }})</span>
        @endif
    </p>

    @if ($comments)
        <ul class="uk-comment-list">
            {!! wp_list_comments([
                'style' => 'ul',
                'short_ping' => true,
                'avatar_size' => 0,
            ], $comments) !!}
        </ul>
    @endif


    <div id="respond" class="comment-respond">
        <h3 id="reply-title" class="comment-reply-title">Dodaj komentarz 
            <small>
                <a rel="nofollow" id="cancel-comment-reply-link" href="/{{ $postSlug }}/#respond" style="display:none;">Anuluj pisanie odpowiedzi</a>
            </small>
        </h3>
        <form action="{{ get_site_url() }}/wp-comments-post.php" method="post" id="commentform"
            class="uk-grid uk-grid-small" novalidate>

            <div class="comment-form-author uk-grid-margin uk-width-1-1 uk-width-1-2@m">
                <input id="author"
                    class="uk-input" name="author" type="text" placeholder="nazwa" value="" size="30">
            </div>
            <div class="comment-form-email uk-grid-margin uk-width-1-1 uk-width-1-2@m">
                <input id="email"
                    class="uk-input" name="email" type="email" placeholder="e-mail" value="" size="30">
            </div>

            <div class="comment-form-comment uk-width-1-1">
                <lt-mirror style="display: none;">
                    <lt-highlighter contenteditable="false" style="display: none;">
                        <lt-div spellcheck="false" class="lt-highlighter__wrapper"
                            style="width: 1198px !important; height: 118px !important; margin-top: 1px !important; margin-left: 1px !important;">
                            <canvas class="lt-highlighter__canvas"
                                style="display: none; margin-top: 0px !important; margin-left: 0px !important;"
                                width="0" height="0">
                            </canvas>
                        </lt-div>
                    </lt-highlighter>
                    <lt-div spellcheck="false" class="lt-mirror__wrapper" data-lt-scroll-top="0"
                        data-lt-scroll-left="0"
                        style="border: 1px solid rgb(221, 221, 221) !important; border-radius: 0px !important; direction: ltr !important; font: 400 13px/13px Montserrat, sans-serif !important; font-feature-settings: normal !important; font-kerning: auto !important; hyphens: manual !important; letter-spacing: normal !important; line-break: auto !important; margin: 0px !important; padding: 12px 10px 4px 25px !important; text-align: start !important; text-decoration: none solid rgb(102, 102, 102) !important; text-indent: 0px !important; text-transform: none !important; word-spacing: 0px !important; overflow-wrap: break-word !important; width: 1163px !important; height: 102px !important;">
                        <lt-div class="lt-mirror__canvas"
                            style="margin-top: 0px !important; margin-left: 0px !important; width: 1163px !important; height: 102px !important;">
                        </lt-div>
                    </lt-div>
                </lt-mirror>
                <textarea id="comment" class="uk-textarea" name="comment"
                    placeholder="komentarz"
                    cols="45" rows="8"
                    aria-required="true"
                    required="required"
                    spellcheck="false"
                    data-gramm="false">
                </textarea>
            </div>


            <p class="form-submit">

                <button
                    class="submit button button--green button--size-big button--empty button--text text-semibold margin-center"
                    >Opublikuj komentarz</button>
                <input type="hidden" name="comment_post_ID" value="{{ get_the_ID() }}" id="comment_post_ID">
                <input type="hidden" name="comment_parent" id="comment_parent" value="0">
            </p>
        </form>
    </div>
</div>
