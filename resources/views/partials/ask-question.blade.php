<div class="uk-container uk-container-expand light-green-bg light-green-bg--size-margin center-flex-it">
    <div class="question-container center-flex-it flex-rev-this">
        <h2 class="footer-title text-bold margin-right">

            Masz pytania?

        </h2>
        <a class="button button--green button--size 
        button--transparent button--text text-semibold" href="#modal-full" data-uk-toggle>

            ZADAJ PYTANIE

        </a>
    </div>
</div>

@include('partials.ask-question-modal')