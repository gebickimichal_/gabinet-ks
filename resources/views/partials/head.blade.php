<head>
    <meta charset="utf-8"/>
	<title>@yield('title')</title>
	<meta name="viewport" content="width=device-width, init-scale=1, shrink-to-fit=no"/>
	<meta name="descrdwtion" content="" />
	<meta name="keywords" content="" />
	<meta name="author" content=""/>
	<meta property="og:title" content="@yield('facebookTitle', 'Gabinet Dietetyki Klinicznej Smosarska')"/>
	<meta property="og:image" content="@yield('facebookImageUrl', '')"/>

	<!-- Favicon -->
	<link rel="shortcut icon" href="@asset('images/favicon.ico')" type="image/x-icon">
	<link rel="icon" href="@asset('images/favicon.ico')" type="image/x-icon">

	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,600,700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.3/css/uikit.min.css" />
	<link rel="stylesheet" href="@asset('styles/app.css')" />
	
	@php(wp_head())
</head>
