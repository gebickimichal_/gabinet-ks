@if ($pagination['numberOfPages'] > 1)
<ul class="uk-pagination uk-flex-right uk-margin-medium-top" data-uk-margin>
    <li>
        {!! get_previous_posts_link('Poprzednia', $pagination['numberOfPages']) !!}
    </li>
    @for ($i = 1; $i <= $pagination['numberOfPages']; $i++)
        <li class="{{ $pagination['currentPageNumber'] === $i ? 'uk-active' : '' }}"><span>{{ $i }}</span></li>
    @endfor
    <li>
        {!! get_next_posts_link('Następna', $pagination['numberOfPages']) !!}
    </li>
</ul>
@endif