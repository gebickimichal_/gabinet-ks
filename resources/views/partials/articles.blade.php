<div class="uk-width-1-1@s uk-width-1-1@m uk-width-1-1@l uk-width-5-6@xl">
    @if (count($articles) > 0)
        @foreach ($articles as $article)
            <div class="uk-grid-collapse uk-margin bottom-border">

                <div class="revert-blog-card" data-uk-grid>

                    <div class="uk-width-1-1@s uk-width-1-2@m uk-width-1-3@l uk-width-1-3@lg uk-card-media-left center-flex">
                        <img src="{{ $article->imageUrl }}" alt="">

                    </div>

                    <div class="uk-width-1-1@s uk-width-1-2@m uk-width-2-3@l uk-width-2-3@lg uk-card-media-left uk-padding-remove-left">
                        <div class="uk-card-body">
                            <p class="post-details text-semibold">
                                <span>{{ date('j.n.Y', strtotime($article->post_date)) }}</span>

                                @foreach ($article->tags as $tag)
                                    <a href="{{ $tag->url }}" class="post-details__tag">{{ mb_strtoupper($tag->name) }}</a>
                                @endforeach
                            </p>
                            
                            @if($article->title)
                                <h3 class="h3">{{ $article->title }}</h3>
                            @endif

                            <p class="text__blog">{!! $article->excerpt !!}</p>

                            <a href="{!! $article->url !!}" class="read-more">czytaj więcej</a>
                        </div>
                    </div>

                </div>
            </div>
        @endforeach
    @else
        <p class="no-results">
          Brak wyników wyszukiwania...
        </p>
    @endif

    @include('partials.pagination')

</div>
