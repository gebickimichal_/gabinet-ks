@extends('layouts.app')

@section('title', $pageTitle)

@section('content')

@include('partials.header-title')

<main>
    <section>
        <div class="uk-container uk-container-large">
            <div class="uk-grid-large" data-uk-grid>

                <div class="uk-width-1-1@s uk-width-1-2@m uk-width-1-2@lg uk-width-1-2@xl">

                    <div class="image-effect-box">
                        <img data-src="{{ $analysisDescriptionImageUrl }}" data-uk-img alt="Mężczyzna obok maszyny do analizy składu ciała"
                            class="uk-preserve image-effect-box--img">
                    </div>

                </div>

                <div class="uk-width-1-1@s uk-width-1-2@m uk-width-1-2@lg uk-width-1-2@xl">

                    <h2 class="section-title text-bold letter-spaceing-50 h2 special-margin">{{ $analysisDescriptionTitle }}</h2>

                    <p class="banner-text-div__first-line text-light letter-spaceing-200 text-green mar-bottom-50">
                        analiza składu ciała</p>

                    <div class="edit-text-box">
                        {!! apply_filters('the_content', $analysisDescription) !!}
                    </div>

                </div>

            </div>
        </div>

        <div class="uk-container uk-container-large container-accordion">
            <ul class="uk-grid" data-uk-grid data-uk-accordion>

                @foreach($analysisInfoLists as $analysisInfoElement)
                <li class="uk-width-1-1 uk-width-1-2@s uk-width-1-2@m uk-width-1-3@l uk-width-1-3@xl">
                    <a class="uk-accordion-title" href="#">
                        <div class="uk-accordion-title__container">
                            {{ $analysisInfoElement['list_title'] }}
                        </div>
                    </a>
                    <div class="uk-accordion-content">
                        <div class="edit-text-box">
                            {!! apply_filters('the_content', $analysisInfoElement['list_elements']) !!}
                        </div>

                    </div>
                </li>
                @endforeach
            </ul>
        </div>

        @include('partials.ask-question')
    </section>
</main>

@endsection
