<?php

namespace App\Fields;

use Carbon_Fields\Container;
use Carbon_Fields\Field;

class PageOmnie {
    public static $postSlug = 'o-mnie';

    public static function getPostId() {
        return get_page_by_path(PageOmnie::$postSlug)->ID;
    }

    public static function generate() {
        Container::make('post_meta', 'O mnie')
            ->where('post_type', '=', 'page')
            ->where('post_id', '=', PageOmnie::getPostId())
                ->add_fields([
                    Field::make('image', 'header_image', 'Zdjęcie w nagłówku strony')
                        ->set_required()
                        ->set_value_type('url') 
                        ->set_width(25),
                    Field::make('image', 'first_image', 'Pierwsze zdjęcie na stronie')
                        ->set_required()
                        ->set_value_type('url')
                        ->set_width(25),
                    Field::make('image', 'second_image', 'Drugie zdjęcie na stronie')
                        ->set_required()
                        ->set_width(25)
                        ->set_value_type('url'),    
                    Field::make('text', 'name', 'Imię i nazwisko')
                        ->set_required()
                        ->set_width(25),
                    Field::make('rich_text', 'sub_name_description', 'Opis')
                        ->set_required(),
                    Field::make('rich_text', 'qualifications', 'Kwalifikacje')
                        ->set_required()
                        ->set_width(50),
                    Field::make('rich_text', 'experience', 'Doświadczenie')
                        ->set_required()
                        ->set_width(50),
                    Field::make('media_gallery', 'certificates_list', 'Certyfikaty')
                        ->set_type('image', 'video')
                        ->set_duplicates_allowed(false)
                ]);
    }
}
