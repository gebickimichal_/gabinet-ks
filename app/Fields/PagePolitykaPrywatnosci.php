<?php

namespace App\Fields;

use Carbon_Fields\Container;
use Carbon_Fields\Field;

class PagePolitykaPrywatnosci {
    public static $postSlug = 'polityka-prywatnosci';

    public static function getPostId() {
        return get_page_by_path(PagePolitykaPrywatnosci::$postSlug)->ID;
    }

    public static function generate() {
        Container::make('post_meta', 'Polityka prywatności')
            ->where('post_type', '=', 'page')
            ->where('post_id', '=', PagePolitykaPrywatnosci::getPostId())
            ->add_fields([
                Field::make('image', 'header_image', 'Zdjęcie w nagłówku strony')
                        ->set_required()
                        ->set_value_type('url'),  
                Field::make('rich_text', 'content', 'Polityka prywatności')
                    ->set_required(),
            ]);
    }
}

