<?php

namespace App\Fields;

use Carbon_Fields\Container;
use Carbon_Fields\Field;

class Przepis {
    public static $postType = 'przepis';

    public static function generate() {
        Container::make('post_meta', 'Przepis')
            ->where('post_type', '=', Przepis::$postType)
            ->add_fields([
                Field::make('image', 'image_url', 'Miniatura przepisu, główna grafika na jego stronie')
                    ->set_required()
                    ->set_value_type('url')
                    ->set_width(25),
                Field::make('rich_text', 'content', 'Treść przepisu')
                    ->set_required()
                    ->set_width(75),
                Field::make('rich_text', 'bibliography', 'Bibliografia')
                    ->set_help_text('Oddzielnie ostylowana sekcja pod przepisem')
            ]);
    }
}
