<?php

namespace App\Fields;

use Carbon_Fields\Container;
use Carbon_Fields\Field;

class ThemeOptions {
    public static function generate() {
        Container::make('theme_options', 'Globalne ustawienia strony')
            ->add_fields([
                Field::make('text', 'telephone_number', 'Numer telefonu')
                    ->set_width(25)
                    ->set_required()
                    ->set_attribute('placeholder', '***-***-***'),
                Field::make('text', 'address', 'Adres')
                    ->set_width(50)
                    ->set_required(),
                Field::make('text', 'email', 'Adres email')
                    ->set_required()
                    ->set_width(25),
                Field::make('text', 'znany_lekarz_url', 'Link do profilu na portalu ZnanyLekarz')
                    ->set_width(50),
                Field::make('text', 'znany_lekarz_widget_url', 'Link do widgetu z portalu ZnanyLekarz')
                    ->set_required()
                    ->set_width(50)
                    ->set_help_text('Link do widgetu, który wyświetli się na stronie \'Umów wizytę\''),
                Field::make('text', 'facebook_page_url', 'Link do strony na facebooku')
                    ->set_width(50),
                Field::make('text', 'instagram_page_url', 'Link do strony na instagramie')
                    ->set_width(50),
                Field::make('text', 'contact_emails', 'Adresy e-mail, na które trafią wiadomości z formularzy kontaktowych na stronie')
                    ->set_required(),
                Field::make('image', 'blog_header_image', 'Zdjęcie w nagłówku strony na blogu')
                    ->set_required()
                    ->set_value_type('url'),
            ]);
    }
}
