<?php

namespace App\Fields;

use Carbon_Fields\Container;
use Carbon_Fields\Field;

class PageFaq {
    public static $postSlug = 'faq';

    public static function getPostId() {
        return get_page_by_path(PageFaq::$postSlug)->ID;
    }

    public static function generate() {
        Container::make('post_meta', 'Faq')
            ->where('post_type', '=', 'page')
            ->where('post_id', '=', PageFaq::getPostId())
            ->add_fields([
                Field::make('image', 'header_image', 'Zdjęcie w nagłówku strony')
                    ->set_required()
                    ->set_value_type('url'),
                Field::make('complex', 'question_answer_list', 'Pytania i odpowiedzi')
                    ->add_fields([
                        Field::make('text', 'question', 'Pytanie')
                            ->set_required()
                            ->set_width(25),
                        Field::make('rich_text', 'answer', 'Odpowiedź')
                            ->set_required()
                            ->set_width(75),
                    ]),
            ]);
            
    }
}
