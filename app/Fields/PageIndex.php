<?php

namespace App\Fields;

use Carbon_Fields\Container;
use Carbon_Fields\Field;

class PageIndex {
    public static $postSlug = 'index';

    public static function getPostId() {
        return get_page_by_path(PageIndex::$postSlug)->ID;
    }

    public static function generate() {
        Container::make('post_meta', 'Index')
            ->where('post_type', '=', 'page')
            ->where('post_id', '=', PageIndex::getPostId())
            ->add_tab('Slajdy', [
                Field::make('complex', 'slides_list', 'Lista slajdów')
                    ->add_fields([
                        Field::make('image', 'image_url', 'Obrazek slajdu')
                            ->set_width(25)
                            ->set_value_type('url')
                            ->set_required(),
                        Field::make('text', 'title', 'Tytuł slajdu')
                            ->set_width(75)
                            ->set_required(),
                    ])
                    ->set_required(),
            ])
            ->add_tab('Opis', [
                Field::make('image', 'description_image_url', 'Grafika obok opisu')
                    ->set_width(25)
                    ->set_value_type('url')
                    ->set_required(),
                Field::make('text', 'name', 'Imię i nazwisko')
                    ->set_width(25)
                    ->set_required(),
                Field::make('rich_text', 'sub_name_description', 'Opis')
                    ->set_width(50)
                    ->set_required(),
            ])
            ->add_tab('Wypunktowane hasła', [
                Field::make('text', 'recipe_title', 'Tytuł sekcji')
                    ->set_required(),
                Field::make('complex', 'list_items', 'Lista')
                    ->add_fields([
                        Field::make('image', 'image_url', 'Zdjęcie obok hasła')
                            ->set_width(25)
                            ->set_value_type('url'),
                        Field::make('text', 'title', 'Hasło reklamowe')
                            ->set_width(75),
                    ])
                    ->set_required(),
            ]);
    }
}
