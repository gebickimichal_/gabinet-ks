<?php

namespace App\Fields;

use Carbon_Fields\Container;
use Carbon_Fields\Field;

class PageAnalizaSkladuCiala {
    public static $postSlug = 'analiza-skladu-ciala';

    public static function getPostId() {
        return get_page_by_path(PageAnalizaSkladuCiala::$postSlug)->ID;
    }

    public static function generate() {
        Container::make('post_meta', 'Analiza składu ciała')
            ->where('post_type', '=', 'page')
            ->where('post_id', '=', PageAnalizaSkladuCiala::getPostId())
            ->add_fields([
                Field::make('image', 'header_image', 'Zdjęcie w nagłówku strony')
                    ->set_required()
                    ->set_value_type('url')
                    ->set_width(35),
                Field::make('image', 'analysis_description_image', 'Zdjęcie obok opisu strony')
                    ->set_required()
                    ->set_value_type('url')
                    ->set_width(35),
                Field::make('text', 'analysis_description_title', 'Tytuł nad opisem strony')
                    ->set_required()
                    ->set_width(30),
                Field::make('rich_text', 'analysis_description', 'Opis metody analizy składu ciała')
                    ->set_required(),
                Field::make('complex', 'analysis_info_lists', 'Listy pod opisem metody analizy składu ciała')
                    ->set_required()
                    ->add_fields([
                        Field::make('text', 'list_title', 'Tytuł listy')
                            ->set_required()
                            ->set_width(25),
                        Field::make('rich_text', 'list_elements', 'Elementy listy')
                            ->set_required()
                            ->set_width(75),
                    ]), 
            ]);
    }
}

