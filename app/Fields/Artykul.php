<?php

namespace App\Fields;

use Carbon_Fields\Container;
use Carbon_Fields\Field;

class Artykul {
    public static $postType = 'artykul';

    public static function generate() {
        Container::make('post_meta', 'Artykul')
            ->where('post_type', '=', Artykul::$postType)
            ->add_fields([
                Field::make('image', 'image_url', 'Miniatura artykułu, główna grafika na jego stronie')
                    ->set_required()
                    ->set_value_type('url')
                    ->set_width(25),
                Field::make('rich_text', 'content', 'Treść artykułu')
                    ->set_required()
                    ->set_width(75),
                Field::make('rich_text', 'bibliography', 'Bibliografia')
                    ->set_help_text('Oddzielnie ostylowana sekcja pod artykułem')
            ]);
    }
}
