<?php

namespace App\Fields;

use Carbon_Fields\Container;
use Carbon_Fields\Field;

class PageUslugi {
    public static $postSlug = 'uslugi';

    public static function getPostId() {
        return get_page_by_path(PageUslugi::$postSlug)->ID;
    }

    public static function generate() {
        Container::make('post_meta', 'Uslugi')
            ->where('post_type', '=', 'page')
            ->where('post_id', '=', PageUslugi::getPostId())
            ->add_fields([
                Field::make('image', 'header_image', 'Zdjęcie w nagłówku strony')
                    ->set_required()
                    ->set_value_type('url'),  
                Field::make('complex', 'service_categories', 'Kategorie usług')
                    ->add_fields([
                        Field::make('text', 'service_category_name', 'Nazwa kategorii usług')
                            ->set_required(),
                        Field::make('complex', 'service_card', 'Usługa')
                            ->add_fields([
                                Field::make('text', 'service_name', 'Nazwa usługi')
                                    ->set_required()
                                    ->set_width(25),
                                Field::make('text', 'service_price', 'Cena usługi')
                                    ->set_required()
                                    ->set_width(25),
                                Field::make('text', 'service_duration', 'Czas trwania usługi')
                                    ->set_help_text('Opcjonalny czas trwania usługi')
                                    ->set_width(25),
                                Field::make('text', 'service_wait_time', 'Czas oczekiwania na realizację')
                                    ->set_help_text('Opcjonalny czas na realizację usługi')
                                    ->set_width(25),
                                Field::make('rich_text', 'service_description_list', 'Lista opisująca usługę')
                                    ->set_help_text('Tutaj podana może zostać wypunktowany opis usługi')
                                    ->set_required()
                            ]),
                    ]),
            ]);
    }
}    
