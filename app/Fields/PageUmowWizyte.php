<?php

namespace App\Fields;

use Carbon_Fields\Container;
use Carbon_Fields\Field;

class PageUmowWizyte {
    public static $postSlug = 'umow-wizyte';

    public static function getPostId() {
        return get_page_by_path(PageUmowWizyte::$postSlug)->ID;
    }

    public static function generate() {
        Container::make('post_meta', 'Umów wizytę')
            ->where('post_type', '=', 'page')
            ->where('post_id', '=', PageUmowWizyte::getPostId())
            ->add_fields([
                Field::make('image', 'header_image', 'Zdjęcie w nagłówku strony')
                    ->set_required()
                    ->set_value_type('url'),  
            ]);
    }
}

