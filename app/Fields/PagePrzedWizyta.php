<?php

namespace App\Fields;

use Carbon_Fields\Container;
use Carbon_Fields\Field;

class PagePrzedWizyta {
    public static $postSlug = 'przed-wizyta';

    public static function getPostId() {
        return get_page_by_path(PagePrzedWizyta::$postSlug)->ID;
    }

    public static function generate() {
        Container::make('post_meta', 'Przed wizytą')
            ->where('post_type', '=', 'page')
            ->where('post_id', '=', PagePrzedWizyta::getPostId())
            ->add_fields([
                Field::make('image', 'header_image', 'Zdjęcie w nagłówku strony')
                    ->set_required()
                    ->set_value_type('url')
                    ->set_width(25),
                Field::make('image', 'pre_appointment_description_image', 'Zdjęcie obok opisu przygotowań do wizyty')
                    ->set_value_type('url')
                    ->set_required()
                    ->set_width(25),
                Field::make('text', 'pre_appointment_description_heading', 'Nagłówek nad opisem przygotowań do wizyty')
                    ->set_width(50)
                    ->set_required(),
                Field::make('rich_text', 'pre_appointment_description_text', 'Opis przygotowań do wizyty')
                    ->set_required(),
                Field::make('complex', 'pre_appointment_actions_list', 'Lista czynności przed wizytą')
                    ->add_fields([
                        Field::make('text', 'action_title', 'Nazwa czynności')
                            ->set_required(),
                        Field::make('rich_text', 'action_text', 'Opis czynności')
                            ->set_required(),
                    ]),
                Field::make('complex', 'pre_appointment_downloads_list', 'Lista plików do pobrania')
                    ->add_fields([
                        Field::make('text', 'file_name', 'Nazwa pliku')
                            ->set_width(25)
                            ->set_required(),
                        Field::make('file', 'file_download_url', 'Plik')
                            ->set_value_type('url')
                            ->set_required()
                            ->set_width(75),
                    ]),
            ]);
    }
}

