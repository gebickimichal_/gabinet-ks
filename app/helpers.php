<?php

/**
 * Theme helpers.
 *
 * @copyright https://roots.io/ Roots
 * @license   https://opensource.org/licenses/MIT MIT
 */

namespace App;

function getParaphase($TEXT_TO_ENCODE = null)
{
    return base64_encode(NONCE_KEY . '{SPACE}' . time() . (!(empty($TEXT_TO_ENCODE)) ? '{SPACE}' . $TEXT_TO_ENCODE : ''));
}

function checkParaphase($hash, $tokenTime = 3600, $TEXT_TO_CHECK = null)
{
    $decoded = base64_decode($hash);
    $split = explode('{SPACE}', $decoded);
    if (!empty($TEXT_TO_CHECK))
        return ($split[0] == NONCE_KEY && ((time() - $split[1] > 0 && time() - $split[1] < $tokenTime) || $tokenTime == null) && (isset($split[2]) && $split[2] == $TEXT_TO_CHECK));
    else
        return ($split[0] == NONCE_KEY && ((time() - $split[1] > 0 && time() - $split[1] < $tokenTime) || $tokenTime == null));
}
