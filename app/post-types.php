<?php

namespace App;

use App\PostTypes\Przepis;
use App\PostTypes\Artykul;

/**
 * Define custom post types
 */
add_action( 'init', function () {
    Artykul::register();
    Przepis::register();
});
