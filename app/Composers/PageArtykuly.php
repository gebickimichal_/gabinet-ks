<?php

namespace App\Composers;

use Roots\Acorn\View\Composer;

class PageArtykuly extends Composer {
    protected $articles;
    protected $paged;
    protected $numberOfPages;
    protected $search;

    const POST_TYPE = 'artykul';
    const TEMPLATE_NAME = 'page-artykuly';
    const TAG_TAXONOMY = 'artykul_tag';
    const CATEGORY_TAXONOMY = 'artykul_category';

    public function __construct() {
        $this->paged = get_query_var('paged') ? get_query_var('paged') : 1;
        $this->search = stripslashes(get_query_var('search'));

        $args = [
            'post_type'      => self::POST_TYPE,
            'paged'          => $this->paged,
            'posts_status'   => 'publish',
            'orderby'        => 'publish_date',
            'order'          => 'DESC'
        ];

        if ($this->search !== '') {
            $args['s'] = $this->search;
        }

        $query = (new \WP_Query($args));
        $this->numberOfPages = $query->max_num_pages;

        $this->articles = $query->posts;
    }

    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        self::TEMPLATE_NAME,
    ];

    public function getPageTitle() {
        return 'Artykuły' . ' - ' . get_bloginfo('name');
    }

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with() {
        return [
            'pageTitle'          => $this->getPageTitle(),
            'headerTitle'        => 'Artykuły',
            'headerImage'        => carbon_get_theme_option('blog_header_image'),
            'articles'           => $this->getArticles(),
            'articlesTags'       => $this->getArticlesTags(),
            'articlesCategories' => $this->getArticlesCategories(),
            'pagination'         => $this->getPaginationData(),
            'searchQuery'        => $this->search,
            'searchUrl'          => $this->getSearchUrl(),
        ];
    }

    public function getSearchUrl() {
        return get_home_url() . '/artykuly/';
    }

    public function getPaginationData() {
        return [
            'currentPageNumber' => $this->paged,
            'numberOfPages'     => $this->numberOfPages,
        ];
    }

    public function getArticlesTags() {
        $tags = get_terms([
            'taxonomy' => self::TAG_TAXONOMY,
            'hide_empty' => false,
        ]);

        foreach ($tags as &$tag) {
            $tag->url = get_term_link($tag->term_id);
        }

        return $tags;
    }

    public function getArticlesCategories() {
        $categories = get_terms([
            'taxonomy' => self::CATEGORY_TAXONOMY,
            'hide_empty' => false,
        ]);

        foreach ($categories as &$category) {
            $category->url = get_term_link($category->term_id);
        }

        return $categories;
    }

    public function getArticleTags($article) {
        $tags = wp_get_post_terms($article->ID, self::TAG_TAXONOMY);

        foreach ($tags as &$tag) {
            $tag->url = get_term_link($tag->term_id);
        }

        return $tags;
    }

    public function getArticles() {
        if ($this->articles) {
            foreach ($this->articles as &$article) {
                $article->title = $article->post_title;
                $article->content = carbon_get_post_meta($article->ID, 'content');
                $article->excerpt = $article->post_excerpt;
                $article->imageUrl = carbon_get_post_meta($article->ID, 'image_url');
                $article->url = get_post_permalink($article->ID);
                $article->tags = $this->getArticleTags($article);
            }
        }

        return $this->articles;
    }
}
