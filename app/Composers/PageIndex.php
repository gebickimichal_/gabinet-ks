<?php

namespace App\Composers;

use Roots\Acorn\View\Composer;

class PageIndex extends Composer {
    protected $postId;

    public function __construct() {
        $this->postId = \App\Fields\PageIndex::getPostId();
    }

    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        'page-index',
    ];

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with() {
        return $this->getIndexPageData();
    }

    public function getPageTitle() {
        return 'Strona główna' . ' - ' . get_bloginfo('name');
    }

    /**
     * Returns page data.
     *
     * @return array
     */
    public function getIndexPageData() {
        return [
            'pageTitle' => $this->getPageTitle(),
            'telephoneNumber' => carbon_get_theme_option('telephone_number'),
            'address' => carbon_get_theme_option('address'),
            'descriptionImageUrl' => carbon_get_post_meta($this->postId, 'description_image_url'),
            'name' => carbon_get_post_meta($this->postId, 'name'),
            'subName' => carbon_get_post_meta($this->postId, 'sub_name'),
            'subNameDescription' => carbon_get_post_meta($this->postId, 'sub_name_description'),
            'recipeTitle' => carbon_get_post_meta($this->postId, 'recipe_title'),
            'listItems' => carbon_get_post_meta($this->postId, 'list_items'),
            'slidesList' => carbon_get_post_meta($this->postId, 'slides_list'),
        ];
    }
}
