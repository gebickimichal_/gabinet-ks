<?php

namespace App\Composers;

use Roots\Acorn\View\Composer;

class PagePrzepisy extends Composer {
    protected $recipes;
    protected $paged;
    protected $numberOfPages;
    protected $search;

    const POST_TYPE = 'przepis';
    const TEMPLATE_NAME = 'page-przepisy';
    const TAG_TAXONOMY = 'przepis_tag';
    const CATEGORY_TAXONOMY = 'przepis_category';

    public function __construct() {
        $this->paged = get_query_var('paged') ? get_query_var('paged') : 1;
        $this->search = stripslashes(get_query_var('search'));

        $args = [
            'post_type'      => self::POST_TYPE,
            'paged'          => $this->paged,
            'posts_status'   => 'publish',
            'orderby'        => 'publish_date',
            'order'          => 'DESC'
        ];

        if ($this->search !== '') {
            $args['s'] = $this->search;
        }

        $query = (new \WP_Query($args));

        $this->numberOfPages = $query->max_num_pages;
        
        $this->recipes = $query->posts;
    }

    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        self::TEMPLATE_NAME,
    ];

    public function getPageTitle() {
        return 'Przepisy' . ' - ' . get_bloginfo('name');
    }

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with() {
        return [
            'pageTitle'         => $this->getPageTitle(),
            'headerTitle'       => 'Przepisy',
            'headerImage'       => carbon_get_theme_option('blog_header_image'),
            'recipes'           => $this->getRecipes(),
            'recipesTags'       => $this->getRecipesTags(),
            'recipesCategories' => $this->getRecipesCategories(),
            'pagination'        => $this->getPaginationData(),
            'searchQuery'       => $this->search,
            'searchUrl'         => $this->getSearchUrl(),
        ];
    }

    public function getSearchUrl() {
        return get_home_url() . '/przepisy/';
    }

    public function getPaginationData() {
        return [
            'currentPageNumber' => $this->paged,
            'numberOfPages'     => $this->numberOfPages,
        ];
    }

    public function getRecipeTags($recipe) {
        $tags = wp_get_post_terms($recipe->ID, self::TAG_TAXONOMY);

        foreach ($tags as &$tag) {
            $tag->url = get_term_link($tag->term_id);
        }

        return $tags;
    }

    public function getRecipesTags() {
        $tags = get_terms([
            'taxonomy' => self::TAG_TAXONOMY,
            'hide_empty' => false,
        ]);

        foreach ($tags as &$tag) {
            $tag->url = get_term_link($tag->term_id);
        }

        return $tags;
    }

    public function getRecipesCategories() {
        $categories = get_terms([
            'taxonomy' => self::CATEGORY_TAXONOMY,
            'hide_empty' => false,
        ]);

        foreach ($categories as &$category) {
            $category->url = get_term_link($category->term_id);
        }

        return $categories;
    }

    public function getRecipes() {
        if ($this->recipes) {
            foreach ($this->recipes as &$recipe) {
                $recipe->title = $recipe->post_title;
                $recipe->content = carbon_get_post_meta($recipe->ID, 'content');
                $recipe->excerpt = $recipe->post_excerpt;
                $recipe->imageUrl = carbon_get_post_meta($recipe->ID, 'image_url');
                $recipe->url = get_post_permalink($recipe->ID);
                $recipe->tags = $this->getRecipeTags($recipe);
            }
        }

        return $this->recipes;
    }
}
