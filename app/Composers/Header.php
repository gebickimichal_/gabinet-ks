<?php

namespace App\Composers;

use Roots\Acorn\View\Composer;

class Header extends Composer {
    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        'partials.header',
    ];

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with() {
        return $this->getHeaderData();
    }

    /**
     * Returns page data.
     *
     * @return array
     */
    public function getHeaderData() {
        return [
            'telephoneNumber' => carbon_get_theme_option('telephone_number'),
            'address' => carbon_get_theme_option('address'),
            'instagramPageUrl' => carbon_get_theme_option('instagram_page_url'),
            'facebookPageUrl' => carbon_get_theme_option('facebook_page_url'),
        ];
    }
}
