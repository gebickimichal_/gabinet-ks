<?php

namespace App\Composers;

use Roots\Acorn\View\Composer;

class PagePolitykaPrywatnosci extends Composer {
    protected $postId;

    public function __construct() {
        $this->postId = \App\Fields\PagePolitykaPrywatnosci::getPostId();
    }

    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        'page-polityka-prywatnosci',
    ];

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with() {
        return $this->getPrivacyPolicy();
    }

    public function getPageTitle() {
        return get_post($this->postId)->post_title . ' - ' . get_bloginfo('name');
    }

    /**
     * Returns page data.
     *
     * @return array
     */
    public function getPrivacyPolicy() {
        return [
            'pageTitle' => $this->getPageTitle(),
            'headerTitle' => 'Polityka prywatności',
            'headerImage' => carbon_get_post_meta($this->postId, 'header_image'),
            'content' => carbon_get_post_meta($this->postId, 'content'),
        ];
    }
}
