<?php

namespace App\Composers;

use Roots\Acorn\View\Composer;

class PageFaq extends Composer {
    protected $postId;

    public function __construct() {
        $this->postId = \App\Fields\PageFaq::getPostId();
    }

    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        'page-faq',
    ];

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with() {
        return $this->getFaqData();
    }

    public function getPageTitle() {
        return get_post($this->postId)->post_title . ' - ' . get_bloginfo('name');
    }

    /**
     * Returns page data.
     *
     * @return array
     */
    public function getFaqData() {
        return [
            'pageTitle' => $this->getPageTitle(),
            'questionsAndAnswers' => carbon_get_post_meta($this->postId, 'question_answer_list'),
            'headerTitle' => 'Faq',
            'headerImage' => carbon_get_post_meta($this->postId, 'header_image'),
        ];
    }
}
