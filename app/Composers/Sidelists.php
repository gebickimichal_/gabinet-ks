<?php

namespace App\Composers;

use Roots\Acorn\View\Composer;

class SideLists extends Composer {
    private $articles;
    private $recipes;

    public function __construct() {
        $args = [
            'post_type'      => 'artykul',
            'posts_per_page' => 3,
            'orderby'        => 'publish_date',
            'order'          => 'DESC'
        ];

        $this->articles = (new \WP_Query($args))->posts;

        $args = [
            'post_type' => 'przepis',
            'posts_per_page' => 3,
            'orderby' => 'publish_date',
            'order' => 'DESC',
        ];

        $this->recipes = (new \WP_Query($args))->posts;
    }

    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        'partials.sidelists',
    ];

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with() {
        return [
            'newRecipesList' => $this->getRecipes(),
            'newArticlesList' => $this->getArticles(),
        ];
    }

    public function getRecipes() {
        if ($this->recipes) {
            foreach ($this->recipes as &$recipe) {
                $recipe->title = $recipe->post_title;
                $recipe->imageUrl = carbon_get_post_meta($recipe->ID, 'image_url');
                $recipe->url = get_post_permalink($recipe->ID);
            }
        }

        return $this->recipes;
    }

    public function getArticles() {
        if ($this->articles) {
            foreach ($this->articles as &$article) {
                $article->title = $article->post_title;
                $article->imageUrl = carbon_get_post_meta($article->ID, 'image_url');
                $article->url = get_post_permalink($article->ID);
            }
        }

        return $this->articles;
    }
}
