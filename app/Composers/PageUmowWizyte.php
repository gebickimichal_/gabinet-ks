<?php

namespace App\Composers;

use Roots\Acorn\View\Composer;

class PageUmowWizyte extends Composer {
    protected $postId;

    public function __construct() {
        $this->postId = \App\Fields\PageUmowWizyte::getPostId();
    }

    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        'page-umow-wizyte',
    ];

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with() {
        return $this->getSetAppointmentPageData();
    }

    public function getPageTitle() {
        return get_post($this->postId)->post_title . ' - ' . get_bloginfo('name');
    }

    /**
     * Returns page data.
     *
     * @return array
     */
    public function getSetAppointmentPageData() {
        return [
            'pageTitle' => $this->getPageTitle(),
            'headerTitle' => 'Umów wizytę',
            'headerImage' => carbon_get_post_meta($this->postId, 'header_image'),
            'telephoneNumber' => carbon_get_theme_option('telephone_number'),
            'znanyLekarzUrl' => carbon_get_theme_option('znany_lekarz_url'),
            'emailAddress' => carbon_get_theme_option('email'),
        ];
    }
}
