<?php

namespace App\Composers;

use Roots\Acorn\View\Composer;

class SingleArtykul extends Composer {
    protected $article;
    protected $comments;
    protected $search;

    const TEMPLATE_NAME = 'single-artykul';
    const POST_TYPE = 'artykul';
    const TAG_TAXONOMY = 'artykul_tag';
    const CATEGORY_TAXONOMY = 'artykul_category';

    function __construct() {
        $this->article = get_queried_object();
        $this->search = stripslashes(get_query_var('search'));

        $this->comments = get_comments([
            'post_id' => $this->article->ID,
            'order' => 'ASC',
        ]);
    } 

    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        self::TEMPLATE_NAME,
    ];

    public function getPageTitle() {
        return $this->article->post_title . ' - ' . get_bloginfo('name');
    }

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with() {
        return [
            'pageTitle' => $this->getPageTitle(),
            'headerTitle' => 'Artykuł',
            'headerImage' => carbon_get_theme_option('blog_header_image'),
            'article' => $this->getArticleData(),
            'articlesTags' => $this->getArticlesTags(),
            'articlesCategories' => $this->getArticlesCategories(),
            'comments' => $this->comments,
            'searchUrl' => $this->getSearchUrl(),
            'searchQuery' => $this->search,
            'postSlug' => $this->article->post_name,
        ];
    }

    public function getSearchUrl() {
        return get_site_url() . '/artykuly/';
    }

    public function getArticleTags($article) {
        $tags = wp_get_post_terms($article->ID, self::TAG_TAXONOMY);

        foreach ($tags as &$tag) {
            $tag->url = get_term_link($tag->term_id);
        }

        return $tags;
    }

    public function getArticlesTags() {
        $tags = get_terms([
            'taxonomy' => self::TAG_TAXONOMY,
            'hide_empty' => false,
        ]);

        foreach ($tags as &$tag) {
            $tag->url = get_term_link($tag->term_id);
        }

        return $tags;
    }

    public function getArticlesCategories() {
        $categories = get_terms([
            'taxonomy' => self::CATEGORY_TAXONOMY,
            'hide_empty' => false,
        ]);

        foreach ($categories as &$category) {
            $category->url = get_term_link($category->term_id);
        }

        return $categories;
    }

    /**
     * Returns page data.
     *
     * @return array
     */
    public function getArticleData() {
        if ($this->article) {
            $this->article->title = $this->article->post_title;
            $this->article->content = carbon_get_post_meta($this->article->ID, 'content');
            $this->article->imageUrl = carbon_get_post_meta($this->article->ID, 'image_url');
            $this->article->bibliography = carbon_get_post_meta($this->article->ID, 'bibliography');
            $this->article->url = get_post_permalink($this->article->ID);
            $this->article->tags = $this->getArticleTags($this->article);
        }

        return $this->article;
    }
}
