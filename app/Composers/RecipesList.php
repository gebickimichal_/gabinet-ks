<?php

namespace App\Composers;

use Roots\Acorn\View\Composer;

class RecipesList extends Composer {
    protected $recipes;
    const POST_TYPE = 'przepis';
    const POSTS_PER_PAGE = 2;

    public function __construct() {
        $args = [
            'post_type'      => self::POST_TYPE,
            'posts_per_page' => self::POSTS_PER_PAGE,
            'orderby'        => 'publish_date',
            'order'          => 'DESC'
        ];

        $this->recipes = (new \WP_Query($args))->posts;
    }

    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        'partials.mainPage.recipes-list',
    ];

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with() {
        return [
            'recipes' => $this->getRecipes(),
            'recipesUrl' => $this->getRecipesArchiveUrl(),
        ];
    }

    public function getRecipesArchiveUrl() {
        return get_site_url() . "/przepisy";
    }

    public function getRecipes() {
        if ($this->recipes) {
            foreach ($this->recipes as &$recipe) {
                $recipe->title = $recipe->post_title;
                $recipe->content = carbon_get_post_meta($recipe->ID, 'content');
                $recipe->excerpt = $recipe->post_excerpt;
                $recipe->imageUrl = carbon_get_post_meta($recipe->ID, 'image_url');
                $recipe->url = get_post_permalink($recipe->ID);
                $recipe->tags = wp_get_post_terms($recipe->ID, 'przepis_tag');
            }
        }

        return $this->recipes;
    }
}
