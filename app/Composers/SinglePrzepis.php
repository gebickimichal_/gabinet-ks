<?php

namespace App\Composers;

use Roots\Acorn\View\Composer;

class SinglePrzepis extends Composer {
    protected $recipe;
    protected $comments;
    protected $search;

    const TEMPLATE_NAME = 'single-przepis';
    const POST_TYPE = 'przepis';
    const TAG_TAXONOMY = 'przepis_tag';
    const CATEGORY_TAXONOMY = 'przepis_category';

    function __construct() {
        $this->recipe = get_queried_object();
        $this->search = stripslashes(get_query_var('search'));

        $this->comments = get_comments([
            'post_id' => $this->recipe->ID,
            'order' => 'ASC',
        ]);

    } 

    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        self::TEMPLATE_NAME,
    ];

    public function getPageTitle() {
        return $this->recipe->post_title . ' - ' . get_bloginfo('name');
    }

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with() {
        return [
            'pageTitle' => $this->getPageTitle(),
            'headerTitle' => 'Przepis',
            'headerImage' => carbon_get_theme_option('blog_header_image'),
            'recipe' => $this->getRecipeData(),
            'recipesTags' => $this->getRecipesTags(),
            'recipesCategories' => $this->getRecipesCategories(),
            'comments' => $this->comments,
            'searchUrl' => $this->getSearchUrl(),
            'searchQuery' => $this->search,
            'postSlug' => $this->recipe->post_name,
        ];
    }

    public function getSearchUrl() {
        return get_site_url() . '/przepisy/';
    }

    public function getRecipeTags($recipe) {
        $tags = wp_get_post_terms($recipe->ID, self::TAG_TAXONOMY);

        foreach ($tags as &$tag) {
            $tag->url = get_term_link($tag->term_id);
        }

        return $tags;
    }

    public function getRecipesTags() {
        $tags = get_terms([
            'taxonomy' => self::TAG_TAXONOMY,
            'hide_empty' => false,
        ]);

        foreach ($tags as &$tag) {
            $tag->url = get_term_link($tag->term_id);
        }

        return $tags;
    }

    public function getRecipesCategories() {
        $categories = get_terms([
            'taxonomy' => self::CATEGORY_TAXONOMY,
            'hide_empty' => false,
        ]);

        foreach ($categories as &$category) {
            $category->url = get_term_link($category->term_id);
        }

        return $categories;
    }

    /**
     * Returns page data.
     *
     * @return array
     */
    public function getRecipeData() {
        if ($this->recipe) {
            $this->recipe->title = $this->recipe->post_title;
            $this->recipe->content = carbon_get_post_meta($this->recipe->ID, 'content');
            $this->recipe->imageUrl = carbon_get_post_meta($this->recipe->ID, 'image_url');
            $this->recipe->bibliography = carbon_get_post_meta($this->recipe->ID, 'bibliography');
            $this->recipe->url = get_post_permalink($this->recipe->ID);
            $this->recipe->tags = $this->getRecipeTags($this->recipe);
        }

        return $this->recipe;
    }
}
