<?php

namespace App\Composers;

use Roots\Acorn\View\Composer;

class PagePrzedWizyta extends Composer {
    protected $postId;

    public function __construct() {
        $this->postId = \App\Fields\PagePrzedWizyta::getPostId();
    }
    
    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        'page-przed-wizyta',
    ];

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with() {
        return $this->getPreAppointmentPageData();
    }

    public function getPageTitle() {
        return get_post($this->postId)->post_title . ' - ' . get_bloginfo('name');
    }

    /**
     * Returns page data.
     *
     * @return array
     */
    public function getPreAppointmentPageData() {
        return [
            'pageTitle' => $this->getPageTitle(),
            'headerTitle' => 'Przed wizytą',
            'headerImage' => carbon_get_post_meta($this->postId, 'header_image'),
            'preAppointmentDescriptionImage' => carbon_get_post_meta($this->postId, 'pre_appointment_description_image'),
            'preAppointmentDescriptionHeading' => carbon_get_post_meta($this->postId, 'pre_appointment_description_heading'),
            'preAppointmentDescriptionText' => carbon_get_post_meta($this->postId, 'pre_appointment_description_text'),
            'preAppointmentActionsList' => carbon_get_post_meta($this->postId, 'pre_appointment_actions_list'),
            'preAppointmentDownloadsList' => carbon_get_post_meta($this->postId, 'pre_appointment_downloads_list'),
        ];
    }
}
