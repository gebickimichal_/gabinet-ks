<?php

namespace App\Composers;

use Roots\Acorn\View\Composer;

class PageUslugi extends Composer {
    protected $postId;

    public function __construct() {
        $this->postId = \App\Fields\PageUslugi::getPostId();
    }

    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        'page-uslugi',
    ];

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with() {
        return $this->getServicesList();
    }

    public function getPageTitle() {
        return get_post($this->postId)->post_title . ' - ' . get_bloginfo('name');
    }

    /**
     * Returns page data.
     *
     * @return array
     */
    public function getServicesList() {
        return [
            'pageTitle' => $this->getPageTitle(),
            'headerTitle' => 'Usługi',
            'headerImage' => carbon_get_post_meta($this->postId, 'header_image'),
            'serviceCategories' => carbon_get_post_meta($this->postId, 'service_categories'),
        ];
    }
}
