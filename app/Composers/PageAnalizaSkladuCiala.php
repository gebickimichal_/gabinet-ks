<?php

namespace App\Composers;

use Roots\Acorn\View\Composer;

class PageAnalizaSkladuCiala extends Composer {
    protected $postId;

    public function __construct() {
        $this->postId = \App\Fields\PageAnalizaSkladuCiala::getPostId();
    }

    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        'page-analiza-skladu-ciala',
    ];

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with() {
        return $this->getBodyAnalysisData();
    }

    public function getPageTitle() {
        return get_post($this->postId)->post_title . ' - ' . get_bloginfo('name');
    }

    /**
     * Returns page data.
     *
     * @return array
     */
    public function getBodyAnalysisData() {
        return [
            'pageTitle' => $this->getPageTitle(),
            'headerImage' => carbon_get_post_meta($this->postId, 'header_image'),
            'headerTitle' => 'Analiza składu ciała',
            'analysisDescriptionImageUrl' => carbon_get_post_meta($this->postId, 'analysis_description_image'),
            'analysisDescriptionTitle' => carbon_get_post_meta($this->postId, 'analysis_description_title'),
            'analysisDescription' => carbon_get_post_meta($this->postId, 'analysis_description'),
            'analysisInfoLists' => carbon_get_post_meta($this->postId, 'analysis_info_lists'),
        ];
    }
}
