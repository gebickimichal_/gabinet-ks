<?php

namespace App\Composers;

use Roots\Acorn\View\Composer;

class PageKontakt extends Composer {
    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        'page-kontakt',
    ];

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with() {
        return $this->getContactData();
    }

    public function getPageTitle() {
        return get_post($this->postId)->post_title . ' - ' . get_bloginfo('name');
    }

    /**
     * Returns page data.
     *
     * @return array
     */
    public function getContactData() {
        return [
            'pageTitle' => $this->getPageTitle(),
            'telephoneNumber' => carbon_get_theme_option('telephone_number'),
            'email' => carbon_get_theme_option('email'),
            'address' => carbon_get_theme_option('address'),
        ];
    }
}
