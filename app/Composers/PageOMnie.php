<?php

namespace App\Composers;

use Roots\Acorn\View\Composer;

class PageOMnie extends Composer {
    protected $postId;

    public function __construct() {
        $this->postId = \App\Fields\PageOMnie::getPostId();
    }

    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        'page-o-mnie',
    ];

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with() {
        return $this->getAboutMeData();
    }

    public function getPageTitle() {
        return get_post($this->postId)->post_title . ' - ' . get_bloginfo('name');
    }

    /**
     * Returns page data.
     *
     * @return array
     */
    public function getAboutMeData() {
        return [
            'pageTitle' => $this->getPageTitle(),
            'headerTitle' => 'O mnie',
            'headerImage' => carbon_get_post_meta($this->postId, 'header_image'),
            'firstImage' => carbon_get_post_meta($this->postId, 'first_image'),
            'name' => carbon_get_post_meta($this->postId, 'name'),
            'subNameDescription' => carbon_get_post_meta($this->postId, 'sub_name_description'),
            'secondImage' => carbon_get_post_meta($this->postId, 'second_image'),
            'qualifications' => carbon_get_post_meta($this->postId, 'qualifications'),
            'experience' => carbon_get_post_meta($this->postId, 'experience'),
            'certificatesList' => carbon_get_post_meta($this->postId, 'certificates_list'),
        ];
    }
}
