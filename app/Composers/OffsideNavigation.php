<?php

namespace App\Composers;

use Log1x\Navi\NaviFacade as Navi;
use Roots\Acorn\View\Composer;

class OffsideNavigation extends Composer {
    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        'partials.navigations.offside-navigation',
    ];

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with() {
        return [
            'navigationElements' => $this->getNavigationElements(),
            'telephoneNumber' => carbon_get_theme_option('telephone_number'),
            'address' => carbon_get_theme_option('address'),
        ];
    }

    /**
     * Returns the primary navigation.
     *
     * @return array
     */
    public function getNavigationElements() {
        return Navi::build('header-navigation')->toArray();
    }
}
