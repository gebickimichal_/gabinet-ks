<?php

namespace App\Composers;

use Roots\Acorn\View\Composer;

class ArticlesList extends Composer {
    protected $articles;
    const POST_TYPE = 'artykul';
    const POSTS_PER_PAGE = 2;

    public function __construct() {
        $args = [
            'post_type'      => self::POST_TYPE,
            'posts_per_page' => self::POSTS_PER_PAGE,
            'orderby'        => 'publish_date',
            'order'          => 'DESC'
        ];

        $this->articles = (new \WP_Query($args))->posts;
    }

    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        'partials.mainPage.articles-list',
    ];

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with() {
        return [
            'articles' => $this->getArticles(),
            'articlesUrl' => get_site_url() . '/artykuly',
        ];
    }

    public function getArticles() {
        if ($this->articles) {
            foreach ($this->articles as &$article) {
                $article->title = $article->post_title;
                $article->content = carbon_get_post_meta($article->ID, 'content');
                $article->excerpt = $article->post_excerpt;
                $article->imageUrl = carbon_get_post_meta($article->ID, 'image_url');
                $article->url = get_post_permalink($article->ID);
                $article->tags = wp_get_post_terms($article->ID, 'artykul_tag');
            }
        }

        return $this->articles;
    }
}
