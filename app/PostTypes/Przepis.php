<?php

namespace App\PostTypes;

class Przepis {
    const POST_TYPE = 'przepis';
    const POST_SLUG = 'przepis';
    const CATEGORY_TAXONOMY = 'przepis_category';
    const CATEGORY_SLUG = 'przepisy-kategorie';
    const TAG_TAXONOMY = 'przepis_tag';
    const TAG_SLUG = 'przepisy-tagi';

    public static function register() {
        register_post_type(
            self::POST_TYPE,
            [
                'labels'                => [
                    'name'               => __( 'Przepisy', 'vp' ),
                    'singular_name'      => __( 'Przepisy', 'vp' ),
                    'all_items'          => __( 'Wszystkie przepisy', 'vp' ),
                    'add_new'            => __( 'Dodaj przepis', 'vp' ),
                    'add_new_item'       => __( 'Dodaj nowy przepis', 'vp' ),
                    'edit'               => __( 'Edytuj', 'vp' ),
                    'edit_item'          => __( 'Edytuj przepis', 'vp' ),
                    'new_item'           => __( 'Nowy przepis', 'vp' ),
                    'search_items'       => __( 'Szukaj przepisu', 'vp' ),
                    'not_found'          => __( 'Nie znaleziono w bazie', 'vp' ),
                    'not_found_in_trash' => __( 'Nie znaleziono w koszu', 'vp' ),
                    'parent_item_colon'  => '',
                ],
                'public'                => true,
                'publicly_queryable'    => true,
                'exclude_from_search'   => false,
                'show_ui'               => true,
                'query_var'             => true,
                'menu_position'         => 8,
                'menu_icon'             => 'dashicons-hammer',
                'has_archive'           => true,
                'supports'              => [ 'title', 'comments', 'excerpt' ],
                'rewrite'               => [ 'slug' => self::POST_SLUG ],
            ]);

        self::registerTaxonomies();
    }

    public function registerTaxonomies() {
        register_taxonomy( 
            self::TAG_TAXONOMY, // taxonomy slug 
            self::POST_TYPE, // post type slug 
            array( 
                'hierarchical' => false, 
                'labels'        => array(
                    'name' => 'Tagi przepisów', 'taxonomy general name',
                    'singular_name' => 'Tag', 'taxonomy singular name',
                    'search_items' =>  'Wyszukaj tagi',
                    'popular_items' => 'Najpopularniejsze tagi',
                    'all_items' => 'Wszystkie tagi',
                    'most_used_items' => null,
                    'parent_item' => null,
                    'parent_item_colon' => null,
                    'edit_item' => 'Edytuj tagi',
                    'update_item' => 'Aktualizuj tagi',
                    'add_new_item' => 'Dodaj nowy tag',
                    'new_item_name' => 'Nazwa nowego tagu',
                    'separate_items_with_commas' => 'Oddziel nazwy przecinkiem',
                    'add_or_remove_items' => 'Dodaj lub usuń tagi',
                    'choose_from_most_used' => 'Wybierz spośród najczęściej używanych tagów',
                    'menu_name' => 'Tagi',
                ), 
                'public'       => true, 
                'show_ui'      => true, 
                'query_var'    => true, 
                'rewrite'      => array(
                    'slug' => self::TAG_SLUG, 
                    'with_front' => false 
                ) 
            ) 
        );
        register_taxonomy_for_object_type(self::TAG_TAXONOMY, self::POST_TYPE);

        register_taxonomy( 
            self::CATEGORY_TAXONOMY, // taxonomy slug 
            self::POST_TYPE, // post type slug 
            array( 
                'hierarchical' => true, 
                'labels'        => array(
                    'name' => 'Kategorie przepisów', 'taxonomy general name',
                    'singular_name' => 'Kategoria', 'taxonomy singular name',
                    'search_items' =>  'Wyszukaj kategorie',
                    'popular_items' => 'Najpopularniejsze kategorie',
                    'all_items' => 'Wszystkie kategorie',
                    'most_used_items' => null,
                    'parent_item' => null,
                    'parent_item_colon' => null,
                    'edit_item' => 'Edytuj kategorie',
                    'update_item' => 'Aktualizuj kategorie',
                    'add_new_item' => 'Dodaj nową kategorie',
                    'new_item_name' => 'Nazwa nowej kategorii',
                    'separate_items_with_commas' => 'Oddziel nazwy przecinkiem',
                    'add_or_remove_items' => 'Dodaj lub usuń kategorie',
                    'choose_from_most_used' => 'Wybierz spośród najczęściej używanych kategorii',
                    'menu_name' => 'Kategorie',
                ),
                'public'       => true, 
                'show_ui'      => true, 
                'query_var'    => true, 
                'rewrite'      => array(
                    'slug' => self::CATEGORY_SLUG, 
                    'with_front' => false,
                ) 
            ) 
        );

        register_taxonomy_for_object_type(self::CATEGORY_TAXONOMY, self::POST_TYPE);
    }
}
