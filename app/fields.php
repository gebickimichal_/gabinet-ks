<?php

namespace App;

use App\Fields\PageIndex;
use App\Fields\PageOMnie;
use App\Fields\PageFaq;
use App\Fields\ThemeOptions;
use App\Fields\Artykul;
use App\Fields\PageAnalizaSkladuCiala;
use App\Fields\PagePolitykaPrywatnosci;
use App\Fields\Przepis;
use App\Fields\PageUslugi;
use App\Fields\PagePrzedWizyta;
use App\Fields\PageUmowWizyte;

/**
 * Define custom fields
 * Docs: https://carbonfields.net/docs/
 */
add_action('carbon_fields_register_fields', function() {
    ThemeOptions::generate();
    
    PageIndex::generate();
    PageOMnie::generate();
    PageFaq::generate();
    PageUslugi::generate();
    PagePrzedWizyta::generate();
    PageAnalizaSkladuCiala::generate();
    PageUmowWizyte::generate();
    PagePolitykaPrywatnosci::generate();
    
    Przepis::generate();
    Artykul::generate();
});

/**
 * Boot Carbon Fields
 */
add_action('after_setup_theme', function() {
    \Carbon_Fields\Carbon_Fields::boot();
});
