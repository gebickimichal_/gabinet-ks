<?php

/**
 * Theme setup.
 *
 * @copyright https://roots.io/ Roots
 * @license   https://opensource.org/licenses/MIT MIT
 */

namespace App;

use function Roots\asset;

/**
 * Register the theme assets.
 *
 * @return void
 */
add_action('wp_enqueue_scripts', function () {
    wp_enqueue_script( 'google-maps', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyD6hIMIfE0j7ttgCGKjL2_OvHKyf6YD5bE', [ 'sage/app' ], null, true );

    wp_enqueue_script('validate', asset('scripts/validation.js')->uri(), [], null, true);
    
    wp_enqueue_script('menu-nav', asset('scripts/menu-nav.js')->uri(), [], null, true);

    wp_enqueue_script( 'sage/vendor', asset( 'scripts/vendor.js' )->uri(), [ 'jquery' ], null, true );
    wp_enqueue_script( 'sage/app', asset( 'scripts/app.js' )->uri(), [ 'sage/vendor', 'jquery' ], null, true );

    wp_add_inline_script( 'sage/vendor', asset( 'scripts/manifest.js' )->contents(), 'before' );

    if (is_single() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }

    wp_enqueue_style('slideToUnlock', asset( 'styles/slideToUnlock.css' )->uri(), false, null );
    wp_enqueue_style('sage/app.css', asset('styles/app.css')->uri(), false, null);

    $google_maps = [
        'marker' => asset( 'images/svg/marker.svg' )->uri(),
    ];
    
    wp_localize_script( 'google-maps', 'map_data', $google_maps );

    $global_data = [
        'send_contact_form_url' => get_site_url() . '/wp-admin/admin-ajax.php?action=send_contact_form',
        'send_question_form_url' => get_site_url() . '/wp-admin/admin-ajax.php?action=send_question_form',
    ];
    wp_localize_script( 'sage/app', 'global_data', $global_data );
}, 100);

/**
 * Register the theme assets with the block editor.
 *
 * @return void
 */
add_action('enqueue_block_editor_assets', function () {
    if ($manifest = asset('scripts/manifest.asset.php')->get()) {
        wp_enqueue_script(
            'sage/editor.js',
            asset('scripts/editor.js')->uri(),
            $manifest['dependencies'],
            $manifest['version']
        );
        
        wp_add_inline_script('sage/editor.js', asset('scripts/manifest.js')->contents(), 'before');
    }

    wp_enqueue_style('sage/editor.css', asset('styles/editor.css')->uri(), false, null);
}, 100);

remove_action( 'wp_head', '_wp_render_title_tag', 1 );

/**
 * Register the initial theme setup.
 *
 * @return void
 */
add_action('after_setup_theme', function () {
    /**
     * Enable features from Soil when plugin is activated
     * @link https://roots.io/plugins/soil/
     */
    add_theme_support('soil-clean-up');
    add_theme_support('soil-nav-walker');
    add_theme_support('soil-nice-search');
    add_theme_support('soil-relative-urls');

    /**
     * Enable plugins to manage the document title
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#title-tag
     */
    add_theme_support('title-tag');

    /**
     * Register navigation menus
     * @link https://developer.wordpress.org/reference/functions/register_nav_menus/
     */
    register_nav_menus([
        'header-navigation' => 'Header menu',
        'footer-navigation' => 'Footer menu',
    ]);

    /**
     * Enable post thumbnails
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support('post-thumbnails');

    /**
     * Add theme support for Wide Alignment
     * @link https://wordpress.org/gutenberg/handbook/designers-developers/developers/themes/theme-support/#wide-alignment
     */
    add_theme_support('align-wide');

    /**
     * Enable responsive embeds
     * @link https://wordpress.org/gutenberg/handbook/designers-developers/developers/themes/theme-support/#responsive-embedded-content
     */
    add_theme_support('responsive-embeds');

    /**
     * Enable HTML5 markup support
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#html5
     */
    add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

    /**
     * Enable selective refresh for widgets in customizer
     * @link https://developer.wordpress.org/themes/advanced-topics/customizer-api/#theme-support-in-sidebars
     */
    add_theme_support('customize-selective-refresh-widgets');
}, 20);

/**
 * Register the theme sidebars.
 *
 * @return void
 */
add_action('widgets_init', function () {
    $config = [
        'before_widget' => '<section class="widget %1$s %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ];

    register_sidebar([
        'name' => __('Primary', 'sage'),
        'id' => 'sidebar-primary'
    ] + $config);

    register_sidebar([
        'name' => __('Footer', 'sage'),
        'id' => 'sidebar-footer'
    ] + $config);
});

/**
 * Move scripts from wp_head() to wp_footer()
 */
add_action( 'wp_enqueue_scripts', function () {
    remove_action( 'wp_head', 'wp_print_scripts' );
    remove_action( 'wp_head', 'wp_print_head_scripts', 9 );
    remove_action( 'wp_head', 'wp_enqueue_scripts', 1 );

    add_action( 'wp_footer', 'wp_print_scripts', 5 );
    add_action( 'wp_footer', 'wp_enqueue_scripts', 5 );
    add_action( 'wp_footer', 'wp_print_head_scripts', 5 );
} );

remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );