<?php

namespace App\Api;

class GetWidget {
    public function __construct() {
        add_action( 'wp_ajax_nopriv_get_widget', [ $this, 'getWidget' ] );
        add_action( 'wp_ajax_get_widget', [ $this, 'getWidget' ] );
    }

    public function getWidget() {
        $widgetUrl = carbon_get_theme_option('znany_lekarz_widget_url');

        $siteUrl = get_site_url();

        $current = "{$siteUrl}/wp-content/themes/smosarska/dist";
    
        $link = "<link rel='stylesheet' href='{$current}/styles/widget.css' /><link rel='stylesheet' href='{$current}/styles/app.css' />"; 
        
        $content = file_get_contents($widgetUrl); 
        $content = str_replace('</title>','</title><base href="https://www.znanylekarz.pl/" />', $content); 
        $content = str_replace('</head>', $link, $content); 

        die($content);
    }
}
