<?php

namespace App\Api;

class SendContactForm {
    public function __construct() {
        add_action( 'wp_ajax_nopriv_send_contact_form', [ $this, 'sendContactForm' ] );
        add_action( 'wp_ajax_send_contact_form', [ $this, 'sendContactForm' ] );
    }

    public function sendContactForm() {
        $translated_names = [
            'name'    => 'Imię i nazwisko:',
            'email'   => 'Adres email:',
            'telephone'   => 'Telefon:',
            'subject' => 'Temat:',
            'message' => '<br>Treść wiadomości:<br>',
        ];
        $mail_data        = $_POST['mail_data'];
        $data             = [];
        foreach ( $mail_data as $item ) {
            $data[ $item['name'] ] = $item['value'];
        };

        if (filter_var( $data['email'], FILTER_VALIDATE_EMAIL === false)) {
            die(json_encode(['result' => 'error', 'msg' => 'Niepoprawny adres email']));
        }

        $content  = "Wiadomość z formularza kontaktowego: <br><br>";
        $subject  = $data['subject'] ? $data['subject'] : "Wiadomość z formularza kontaktowego";

        foreach ( $translated_names as $key => $name ) {
            if ( key_exists( $key, $data ) ) {
                $content .= $name . " " . $data[ $key ] . "<br>";
            }
        }

        $recipients = explode(',', carbon_get_theme_option('contact_emails'));

        $headers   = [
            'Content-Type: text/html; charset=UTF-8',
            'From: Formularz kontaktowy klaudiasmosarska.pl <' . $recipients[0] . '>',
            'Reply-To: ' . $data['name'] . ' <' . $data['email'] . '>',
        ];

        wp_mail( $recipients, $subject, $content, $headers );

        $result = [
            'result' => 'success',
            'message'    => 'Twoja wiadomość zostałą wysłana! Dziękujęmy!',
        ];
        die(json_encode($result));
    }
}