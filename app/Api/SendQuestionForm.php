<?php

namespace App\Api;

class SendQuestionForm {
    public function __construct() {
        add_action( 'wp_ajax_nopriv_send_question_form', [ $this, 'sendQuestionForm' ] );
        add_action( 'wp_ajax_send_question_form', [ $this, 'sendQuestionForm' ] );
    }

    public function sendQuestionForm() {
        $translated_names = [
            'client_type' => 'Typ klienta: ',
            'name'    => 'Imię i nazwisko: ',
            'email'   => 'Adres email: ',
            'telephone'   => 'Telefon: ',
            'message' => '<br>Treść wiadomości:<br>',
        ];

        $mail_data        = $_POST['mail_data'];
        $data             = [];
        foreach ( $mail_data as $item ) {
            $data[ $item['name'] ] = $item['value'];
        };

        if (filter_var( $data['email'], FILTER_VALIDATE_EMAIL === false)) {
            die(json_encode(['result' => 'error', 'msg' => 'Niepoprawny adres email']));
        }
       
        $content  = "Wiadomość z formularza 'zadaj pytanie': <br><br>";
        $subject  = "Wiadomość z formularza 'zadaj pytanie'";

        foreach ( $translated_names as $key => $name ) {
            if ( key_exists( $key, $data ) ) {
                $content .= $name . " " . $data[ $key ] . "<br>";
            }
        }

        $recipients = explode(',', carbon_get_theme_option('contact_emails'));

        $headers = [
            'Content-Type: text/html; charset=UTF-8',
            'From: Formularz z zapytaniem klaudiasmosarska.pl <' . $recipients[0] . '>',
            'Reply-To: ' . $data['name'] . ' <' . $data['email'] . '>',
        ];

        wp_mail( $recipients, $subject, $content, $headers );

        $result = [
            'result' => 'success',
            'message'    => 'Twoja wiadomość zostałą wysłana! Dziękujęmy!',
        ];
        die( json_encode( $result ) );
    }
}