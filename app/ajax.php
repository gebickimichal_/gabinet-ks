<?php

namespace App;

use App\Api\GetWidget;
use App\Api\SendContactForm;
use App\Api\SendQuestionForm;

new GetWidget();
new SendContactForm();
new SendQuestionForm();
