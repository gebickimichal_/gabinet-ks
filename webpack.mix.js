const mix = require('laravel-mix');
            require('laravel-mix-wp-blocks');
            require('laravel-mix-purgecss');
            require('laravel-mix-copy-watched');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Sage application. By default, we are compiling the Sass file
 | for your application, as well as bundling up your JS files.
 |
 */

// Source path helper
const src = path => `resources/assets/${path}`;

mix.setPublicPath('./dist')
   .browserSync('sage.test');

mix.sass(src`styles/app.scss`, 'styles')
   .sass(src`styles/style-login.scss`, 'styles');

mix.js(src`scripts/app.js`, 'scripts')
   .js(src`scripts/customizer.js`, 'scripts')
   .js(src`scripts/map.js`, 'scripts')
   .js(src`scripts/menu-nav.js`, 'scripts')
   .js(src`scripts/validation.js`, 'scripts')
   .extract();

mix.copyWatched(src`images`, 'dist/images')
   .copyWatched(src`fonts`, 'dist/fonts')
   .copyWatched(src`images/svg`, 'dist/images/svg')
   .copyWatched(src`styles/widget.css`, 'dist/styles')
   .copyWatched(src`styles/slideToUnlock.css`, 'dist/styles');

mix.autoload({
  jquery: ['$', 'window.jQuery'],
});

mix.options({
  processCssUrls: false,
});

mix.sourceMaps(false, 'source-map')
   .version();
